// banner voor cuisine. op nieuws.html
(function(cjs, an) {

    var p; // shortcut to reference prototypes
    var lib = {};
    var ss = {};
    var img = {};
    lib.ssMetadata = [];

    // symbols:

    (lib.jamie1 = function() {
        this.initialize(img.jamie1);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 710);


    (lib.jamie2 = function() {
        this.initialize(img.jamie2);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 721);


    (lib.jamie3 = function() {
        this.initialize(img.jamie3);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 710);


    (lib.jamie4 = function() {
        this.initialize(img.jamie4);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 713);


    (lib.jamie5 = function() {
        this.initialize(img.jamie5);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 722);


    (lib.nieuwsherojamieoliver = function() {
        this.initialize(img.nieuwsherojamieoliver);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 1920, 1080);


    (lib.nigella1 = function() {
        this.initialize(img.nigella1);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 741);


    (lib.nigella2 = function() {
        this.initialize(img.nigella2);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 713);


    (lib.nigella3 = function() {
        this.initialize(img.nigella3);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 633);


    (lib.nigella4 = function() {
        this.initialize(img.nigella4);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 710);


    (lib.nigella5 = function() {
        this.initialize(img.nigella5);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 550, 733);


    (lib.nigellalawson = function() {
        this.initialize(img.nigellalawson);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 3005, 2000); // helper functions:

    function mc_symbol_clone() {
        var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
        clone.gotoAndStop(this.currentFrame);
        clone.paused = this.paused;
        clone.framerate = this.framerate;
        return clone;
    }

    function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
        var prototype = cjs.extend(symbol, cjs.MovieClip);
        prototype.clone = mc_symbol_clone;
        prototype.nominalBounds = nominalBounds;
        prototype.frameBounds = frameBounds;
        return prototype;
    }


    (lib.Tween16 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgJBaIgJgGQgDgDgDgEQgCgGAAgFQAAgGACgFQADgEADgEIAJgFQAEgDAFAAQAGAAAEADIAJAFQAEAEACAEQACAFAAAGQAAAFgCAGQgCAEgEADIgJAGQgEACgGAAQgFAAgEgCgAgJgoIgJgGQgDgEgDgEQgCgFAAgGQAAgGACgEIAGgJIAJgGQAEgBAFAAQAGAAAEABIAJAGQAEAEACAFQACAEAAAGQAAAGgCAFQgCAEgEAEIgJAGQgEACgGAAQgFAAgEgCg");
        this.shape.setTransform(142.2, -4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_1.setTransform(124.7, -5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IApgWIASAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFgBgGQABgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQACAFAAAHQAAAGgCAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_2.setTransform(106, -10);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AgNCBQgJgDgHgGQgGgHgDgJQgDgJAAgLIAAiAIggAAIAAgPIAggKIAAgpIApgWIASAAIAAA/IAxAAIAAAZIgxAAIAAB/QAAAFAEAFQADAFAFgBIAHgCIAGgFIAGgGIAFgEIAUAQIgLAPQgGAHgIAEQgHAFgLADQgKADgNAAQgMAAgJgEg");
        this.shape_3.setTransform(91, -8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AgkBfQgTgIgOgNQgMgOgIgSQgIgSAAgUQAAgOAEgNQADgOAIgMQAGgMAKgKQAJgKAMgHQAMgHAOgEQANgDANAAQAPAAAMACQALABAJAFQAJAEAIAHIAOARIgjAaIgEgJQgCgGgFgHQgFgGgIgFQgJgFgMAAQgKAAgHAFQgIAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQAEAPAGAMQAIAMAJAGQAIAHANAAQAJAAAJgEQAHgEAHgHQAHgGAGgKIAKgUIAcAIQgFAUgJAPQgJAPgMAKQgNALgPAGQgQAFgSAAQgUAAgRgIg");
        this.shape_4.setTransform(70.3, -5.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_5.setTransform(46, -5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_6.setTransform(27.3, -10.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_7.setTransform(14.6, -10.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AgrBfQgUgIgPgNQgOgOgJgSQgIgTAAgVQAAgWAIgTQAJgUAOgOQAPgOAUgHQAUgIAXAAQAXAAAVAHQAUAIAPANQAOAOAJASQAIASAAAVQAAAVgIAUQgIATgPAPQgPAOgUAIQgUAJgYAAQgXAAgUgIgAgVhJQgHAFgFAJQgFAJgDALQgDAMAAANQAAASAEARQADARAHANQAGANAKAHQAKAIALAAQAIAAAHgGQAHgFAFgJQAFgJADgMQADgMAAgNQAAgSgDgRQgDgQgGgNQgGgNgLgHQgKgIgOAAQgHAAgGAGg");
        this.shape_8.setTransform(-4.8, -5.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AglBfQgSgIgNgNQgNgOgJgSQgHgSAAgUQAAgOAEgNQADgOAHgMQAHgMAKgKQAKgKALgHQAMgHANgEQANgDAOAAQAQAAALACQALABAJAFQAJAEAHAHIAPARIgjAaIgEgJQgCgGgFgHQgFgGgJgFQgIgFgMAAQgKAAgHAFQgIAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQADAPAIAMQAGAMAKAGQAJAHAMAAQAJAAAJgEQAHgEAIgHQAGgGAGgKIAJgUIAdAIQgFAUgJAPQgJAPgNAKQgMALgQAGQgPAFgSAAQgUAAgSgIg");
        this.shape_9.setTransform(-29.9, -5.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AggBjQgMgEgJgHQgJgGgGgJIgKgQIAigOQAGAQAJAKQAKAJAPAAIAKgBIAJgFIAGgGQACgFAAgDQAAgIgCgFQgDgGgFgEIgLgJIgNgGIgXgMQgMgGgJgHQgKgIgFgKQgGgKAAgOQAAgMAFgKQAGgKAJgIQAJgHAOgFQANgDAQAAQAOgBALADQALADAIAFQAJAFAHAHQAGAIAFAIIgjANQgCgKgEgFIgIgIIgIgDIgIAAIgJABIgJADIgHAGQgCAEAAAEQAAAGADAFQADAEAFAFIAMAIIAPAIIAZALQAMAGAJAIQAJAIAGAKQAGAJgBAMQABAOgFALQgEALgKAJQgJAHgPAFQgQAGgVAAQgTgBgNgEg");
        this.shape_10.setTransform(-61.1, -5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AgFA6QgBgLgEgLIgIgVIgHgVQgDgKAAgMQAAgGACgFQADgFADgEIAKgHQAFgCAFAAQAGAAAFACQAGADAEAEQADAEADAFQACAFAAAGQAAAMgDAKIgHAVIgIAVQgDALgBALg");
        this.shape_11.setTransform(-75.1, -15.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_12.setTransform(-91.5, -5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IAqgWIASAAIAADKgAgMhbIgKgHQgEgDgDgGQgDgFABgGQgBgHADgFQADgFAEgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQADAFgBAHQABAGgDAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_13.setTransform(-110.2, -10);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFFFFF").s().p("ABcBmIAAiGIgCgNIgEgKIgHgHQgEgDgEAAIgLACIgLAEIgLAIIgJAIIAACRIg6AAIAAiGIgCgNIgFgKIgHgHQgDgDgFAAIgKACIgLAEIgKAIIgKAIIAACRIg8AAIAAi0IAqgXIASAAIAAAWQAOgKAPgGQAQgFAOAAIAPACIAQAEIAMAIQAHAFAEAGIAPgLIARgHIASgFIATgCQANAAAMAFQALAFAIAIQAJAJAFAMQAFAKAAAOIAACLg");
        this.shape_14.setTransform(-136.2, -5.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AhABkQgMgEgHgHQgIgHgEgKQgEgJAAgLQAAgRAJgMQAKgNAQgJQAPgIAUgHIAogNIAAgTQAAgKgCgGQgCgHgEgEQgDgEgEgCIgLgCQgHAAgGACQgGADgEAFIgGALIgEAMIgzAAQADgMAIgKQAIgLALgHQAMgHAPgEQAPgEARAAQAUAAAQAEQAQAEAJAHQALAHAEAKQAFAKAAANIAABxQAAAMAIAJQAHAIAOAGIhHAAIgHgCIgFgFIgEgGIgCgGIgLAJIgJAHIgNAFIgSACQgOAAgKgEgAgGAGQgJAFgHAHQgHAGgFAIQgEAIAAAJQAAAKAFAGQAFAFAJAAQAHAAAHgEIAQgMIAAg4g");
        this.shape_15.setTransform(-166.3, -5.1);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#FFFFFF").s().p("AgfCDIgVgGIgSgIIgRgKIATgXIAKAIIANAHIAPAGQAIACAJAAQAQAAAKgJQAKgJAAgPIAAjPIBBAAIAADBQAAAQgGAOQgGANgMAKQgMAKgRAFQgTAGgWAAQgNAAgMgDg");
        this.shape_16.setTransform(-190.4, -8.1);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_16
            }, {
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-201, -34, 402.1, 68.1);


    (lib.Tween15 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgJBaIgJgGQgDgDgDgEQgCgGAAgFQAAgGACgFQADgEADgEIAJgFQAEgDAFAAQAGAAAEADIAJAFQAEAEACAEQACAFAAAGQAAAFgCAGQgCAEgEADIgJAGQgEACgGAAQgFAAgEgCgAgJgoIgJgGQgDgEgDgEQgCgFAAgGQAAgGACgEIAGgJIAJgGQAEgBAFAAQAGAAAEABIAJAGQAEAEACAFQACAEAAAGQAAAGgCAFQgCAEgEAEIgJAGQgEACgGAAQgFAAgEgCg");
        this.shape.setTransform(142.2, -4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_1.setTransform(124.7, -5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IApgWIASAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFgBgGQABgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQACAFAAAHQAAAGgCAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_2.setTransform(106, -10);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AgNCBQgJgDgHgGQgGgHgDgJQgDgJAAgLIAAiAIggAAIAAgPIAggKIAAgpIApgWIASAAIAAA/IAxAAIAAAZIgxAAIAAB/QAAAFAEAFQADAFAFgBIAHgCIAGgFIAGgGIAFgEIAUAQIgLAPQgGAHgIAEQgHAFgLADQgKADgNAAQgMAAgJgEg");
        this.shape_3.setTransform(91, -8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AgkBfQgTgIgOgNQgMgOgIgSQgIgSAAgUQAAgOAEgNQADgOAIgMQAGgMAKgKQAJgKAMgHQAMgHAOgEQANgDANAAQAPAAAMACQALABAJAFQAJAEAIAHIAOARIgjAaIgEgJQgCgGgFgHQgFgGgIgFQgJgFgMAAQgKAAgHAFQgIAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQAEAPAGAMQAIAMAJAGQAIAHANAAQAJAAAJgEQAHgEAHgHQAHgGAGgKIAKgUIAcAIQgFAUgJAPQgJAPgMAKQgNALgPAGQgQAFgSAAQgUAAgRgIg");
        this.shape_4.setTransform(70.3, -5.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_5.setTransform(46, -5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_6.setTransform(27.3, -10.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_7.setTransform(14.6, -10.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AgrBfQgUgIgPgNQgOgOgJgSQgIgTAAgVQAAgWAIgTQAJgUAOgOQAPgOAUgHQAUgIAXAAQAXAAAVAHQAUAIAPANQAOAOAJASQAIASAAAVQAAAVgIAUQgIATgPAPQgPAOgUAIQgUAJgYAAQgXAAgUgIgAgVhJQgHAFgFAJQgFAJgDALQgDAMAAANQAAASAEARQADARAHANQAGANAKAHQAKAIALAAQAIAAAHgGQAHgFAFgJQAFgJADgMQADgMAAgNQAAgSgDgRQgDgQgGgNQgGgNgLgHQgKgIgOAAQgHAAgGAGg");
        this.shape_8.setTransform(-4.8, -5.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AglBfQgSgIgNgNQgNgOgJgSQgHgSAAgUQAAgOAEgNQADgOAHgMQAHgMAKgKQAKgKALgHQAMgHANgEQANgDAOAAQAQAAALACQALABAJAFQAJAEAHAHIAPARIgjAaIgEgJQgCgGgFgHQgFgGgJgFQgIgFgMAAQgKAAgHAFQgIAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQADAPAIAMQAGAMAKAGQAJAHAMAAQAJAAAJgEQAHgEAIgHQAGgGAGgKIAJgUIAdAIQgFAUgJAPQgJAPgNAKQgMALgQAGQgPAFgSAAQgUAAgSgIg");
        this.shape_9.setTransform(-29.9, -5.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AggBjQgMgEgJgHQgJgGgGgJIgKgQIAigOQAGAQAJAKQAKAJAPAAIAKgBIAJgFIAGgGQACgFAAgDQAAgIgCgFQgDgGgFgEIgLgJIgNgGIgXgMQgMgGgJgHQgKgIgFgKQgGgKAAgOQAAgMAFgKQAGgKAJgIQAJgHAOgFQANgDAQAAQAOgBALADQALADAIAFQAJAFAHAHQAGAIAFAIIgjANQgCgKgEgFIgIgIIgIgDIgIAAIgJABIgJADIgHAGQgCAEAAAEQAAAGADAFQADAEAFAFIAMAIIAPAIIAZALQAMAGAJAIQAJAIAGAKQAGAJgBAMQABAOgFALQgEALgKAJQgJAHgPAFQgQAGgVAAQgTgBgNgEg");
        this.shape_10.setTransform(-61.1, -5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AgFA6QgBgLgEgLIgIgVIgHgVQgDgKAAgMQAAgGACgFQADgFADgEIAKgHQAFgCAFAAQAGAAAFACQAGADAEAEQADAEADAFQACAFAAAGQAAAMgDAKIgHAVIgIAVQgDALgBALg");
        this.shape_11.setTransform(-75.1, -15.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_12.setTransform(-91.5, -5);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IAqgWIASAAIAADKgAgMhbIgKgHQgEgDgDgGQgDgFABgGQgBgHADgFQADgFAEgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQADAFgBAHQABAGgDAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_13.setTransform(-110.2, -10);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFFFFF").s().p("ABcBmIAAiGIgCgNIgEgKIgHgHQgEgDgEAAIgLACIgLAEIgLAIIgJAIIAACRIg6AAIAAiGIgCgNIgFgKIgHgHQgDgDgFAAIgKACIgLAEIgKAIIgKAIIAACRIg8AAIAAi0IAqgXIASAAIAAAWQAOgKAPgGQAQgFAOAAIAPACIAQAEIAMAIQAHAFAEAGIAPgLIARgHIASgFIATgCQANAAAMAFQALAFAIAIQAJAJAFAMQAFAKAAAOIAACLg");
        this.shape_14.setTransform(-136.2, -5.3);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AhABkQgMgEgHgHQgIgHgEgKQgEgJAAgLQAAgRAJgMQAKgNAQgJQAPgIAUgHIAogNIAAgTQAAgKgCgGQgCgHgEgEQgDgEgEgCIgLgCQgHAAgGACQgGADgEAFIgGALIgEAMIgzAAQADgMAIgKQAIgLALgHQAMgHAPgEQAPgEARAAQAUAAAQAEQAQAEAJAHQALAHAEAKQAFAKAAANIAABxQAAAMAIAJQAHAIAOAGIhHAAIgHgCIgFgFIgEgGIgCgGIgLAJIgJAHIgNAFIgSACQgOAAgKgEgAgGAGQgJAFgHAHQgHAGgFAIQgEAIAAAJQAAAKAFAGQAFAFAJAAQAHAAAHgEIAQgMIAAg4g");
        this.shape_15.setTransform(-166.3, -5.1);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#FFFFFF").s().p("AgfCDIgVgGIgSgIIgRgKIATgXIAKAIIANAHIAPAGQAIACAJAAQAQAAAKgJQAKgJAAgPIAAjPIBBAAIAADBQAAAQgGAOQgGANgMAKQgMAKgRAFQgTAGgWAAQgNAAgMgDg");
        this.shape_16.setTransform(-190.4, -8.1);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_16
            }, {
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-201, -34, 402.1, 68.1);


    (lib._3JamieBoek5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.jamie5();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._3JamieBoek5, new cjs.Rectangle(0, 0, 550, 722), null);


    (lib._3JamieBoek4 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.jamie4();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._3JamieBoek4, new cjs.Rectangle(0, 0, 550, 713), null);


    (lib._3JamieBoek3 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.jamie3();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._3JamieBoek3, new cjs.Rectangle(0, 0, 550, 710), null);


    (lib._3JamieBoek2 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.jamie2();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._3JamieBoek2, new cjs.Rectangle(0, 0, 550, 721), null);


    (lib._3JamieBoek1 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.jamie1();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._3JamieBoek1, new cjs.Rectangle(0, 0, 550, 710), null);


    (lib.Tween14 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigella1();
        this.instance.parent = this;
        this.instance.setTransform(-130, -175, 0.473, 0.472);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-130, -175, 260, 350);


    (lib.Tween13 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigella1();
        this.instance.parent = this;
        this.instance.setTransform(-130, -175, 0.473, 0.472);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-130, -175, 260, 350);


    (lib.Tween12 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgJBaIgJgGQgDgDgDgEQgCgGAAgFQAAgGACgFQADgEADgEIAJgFQAFgDAEAAQAGAAAEADIAJAFQAEAEACAEQACAFAAAGQAAAFgCAGQgCAEgEADIgJAGQgEACgGAAQgEAAgFgCgAgJgoIgJgGQgDgEgDgEQgCgFAAgGQAAgGACgEIAGgJIAJgGQAFgBAEAAQAGAAAEABIAJAGQAEAEACAFQACAEAAAGQAAAGgCAFQgCAEgEAEIgJAGQgEACgGAAQgEAAgFgCg");
        this.shape.setTransform(146.1, -4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_1.setTransform(128.7, -5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IAqgWIARAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFAAgGQAAgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAFAFACAFQADAFgBAHQABAGgDAFQgCAGgFADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_2.setTransform(109.9, -10);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AgNCBQgJgDgHgGQgGgHgDgJQgDgJAAgLIAAiAIggAAIAAgPIAggKIAAgpIApgWIASAAIAAA/IAxAAIAAAZIgxAAIAAB/QAAAFAEAFQACAFAGgBIAHgCIAGgFIAHgGIADgEIAVAQIgLAPQgGAHgIAEQgHAFgLADQgKADgNAAQgLAAgKgEg");
        this.shape_3.setTransform(95, -8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AgkBfQgTgIgOgNQgNgOgHgSQgIgSAAgUQAAgOADgNQAFgOAHgMQAGgMAJgKQAKgKANgHQALgHAOgEQANgDANAAQAPAAAMACQALABAKAFQAIAEAIAHIAOARIgjAaIgEgJQgCgGgFgHQgFgGgIgFQgJgFgNAAQgIAAgJAFQgHAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQAEAPAGAMQAIAMAJAGQAIAHANAAQAJAAAJgEQAIgEAGgHQAHgGAGgKIAKgUIAcAIQgFAUgJAPQgJAPgMAKQgNALgPAGQgQAFgSAAQgUAAgRgIg");
        this.shape_4.setTransform(74.2, -5.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_5.setTransform(49.9, -5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_6.setTransform(31.3, -10.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_7.setTransform(18.6, -10.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AgrBfQgUgIgPgNQgOgOgJgSQgIgTAAgVQAAgWAIgTQAJgUAOgOQAPgOAUgHQAUgIAXAAQAXAAAVAHQAUAIAPANQAOAOAJASQAIASAAAVQAAAVgIAUQgIATgPAPQgPAOgUAIQgUAJgYAAQgXAAgUgIgAgVhJQgHAFgFAJQgFAJgDALQgDAMAAANQAAASAEARQADARAHANQAGANAKAHQAKAIALAAQAIAAAHgGQAHgFAFgJQAFgJADgMQADgMAAgNQAAgSgDgRQgDgQgGgNQgGgNgLgHQgKgIgOAAQgHAAgGAGg");
        this.shape_8.setTransform(-0.8, -5.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AglBfQgSgIgOgNQgNgOgIgSQgHgSAAgUQAAgOAEgNQADgOAIgMQAGgMAKgKQAJgKAMgHQAMgHAOgEQAMgDAOAAQAQAAALACQAMABAIAFQAJAEAHAHIAPARIgjAaIgEgJQgCgGgFgHQgFgGgJgFQgIgFgMAAQgKAAgHAFQgIAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQADAPAIAMQAGAMAKAGQAJAHAMAAQAKAAAIgEQAHgEAHgHQAHgGAGgKIAKgUIAcAIQgFAUgJAPQgJAPgNAKQgMALgQAGQgPAFgSAAQgUAAgSgIg");
        this.shape_9.setTransform(-25.9, -5.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AgfBjQgNgEgJgHQgJgGgGgJIgKgQIAigOQAFAQAKAKQAKAJAPAAIAKgBIAJgFIAGgGQACgFAAgDQAAgIgCgFQgDgGgFgEIgLgJIgNgGIgXgMQgMgGgJgHQgKgIgFgKQgGgKAAgOQAAgMAFgKQAFgKAKgIQAJgHAOgFQANgDAQAAQAOgBALADQALADAIAFQAJAFAHAHQAHAIAEAIIgjANQgCgKgEgFIgIgIIgIgDIgIAAIgJABIgJADIgGAGQgDAEAAAEQAAAGADAFQADAEAFAFIAMAIIAPAIIAZALQAMAGAJAIQAJAIAGAKQAGAJgBAMQABAOgFALQgEALgKAJQgJAHgPAFQgQAGgVAAQgSgBgNgEg");
        this.shape_10.setTransform(-57.1, -5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AgFA6QgBgLgEgLIgIgVIgHgVQgDgKAAgMQAAgGACgFQADgFADgEIAKgHQAFgCAFAAQAGAAAFACQAFADAFAEQADAEADAFQACAFAAAGQAAAMgDAKIgHAVIgIAVQgDALgBALg");
        this.shape_11.setTransform(-71.1, -15.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("AhABkQgMgEgHgHQgIgHgEgKQgEgJAAgLQAAgRAJgMQAKgNAQgJQAPgIAUgHIAogNIAAgTQAAgKgCgGQgCgHgEgEQgDgEgEgCIgLgCQgHAAgGACQgGADgEAFIgGALIgEAMIgzAAQADgMAIgKQAIgLALgHQAMgHAPgEQAPgEARAAQAUAAAQAEQAQAEAJAHQALAHAEAKQAFAKAAANIAABxQAAAMAIAJQAHAIAOAGIhHAAIgHgCIgFgFIgEgGIgCgGIgLAJIgJAHIgNAFIgSACQgOAAgKgEgAgGAGQgJAFgHAHQgHAGgFAIQgEAIAAAJQAAAKAFAGQAFAFAJAAQAHAAAHgEIAQgMIAAg4g");
        this.shape_12.setTransform(-86.1, -5.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_13.setTransform(-104, -10.2);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_14.setTransform(-116.7, -10.2);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_15.setTransform(-135.2, -5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#FFFFFF").s().p("Ag5CPQgUgDgOgGQgOgGgGgIQgHgIAAgJQAAgHAEgHIAKgNIAPgNIAQgNQgMgEgHgGQgHgFAAgIQAAgFADgGIAIgMIAOgJIAPgJIgSgKQgHgFgGgHQgFgHgDgIQgDgJAAgLQgBgRAIgOQAHgNAOgKQANgKARgFQASgGAVAAIATACIASAEIAOAFIALAHIA9AAIAAAVIglAAIAIAQQADAIAAAKQABARgIANQgIANgNAIQgOAKgSAEQgTAFgWAAIgOgBIgMgCQgEADgDAEQgCAGAAAFIABADIAEACIAJACIAQACIAZACIAlACQATAAAOAEQAOAEAHAGQAJAGAEAHQADAJAAAJQAAAQgJAPQgIAOgSAKQgQAKgYAGQgYAGgdAAQgbAAgVgEgAg1A+IgFAHIgDAIIAAAIQAAAGAEAGQAEAFAJAFQAIAEANADQAMADARAAQARAAAPgDQAPgEAKgFQAKgEAFgGQAGgGgBgGQABgLgMgFQgLgGgTAAIgbgBIgZgBIgWgCIgQgBgAgWh6QgGAEgEAHQgDAGgCAJIgCASQAAAMADALQACAKAGAIQAEAIAHAFQAIAFAIAAQAGAAAGgEQAGgDAFgHQAFgGADgIQADgIAAgJQAAgNgEgLQgDgLgGgIQgFgJgIgFQgHgFgIAAQgJAAgFAEg");
        this.shape_16.setTransform(-159.3, -0.8);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IApgWIASAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFgBgGQABgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQACAFAAAHQAAAGgCAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_17.setTransform(-178.3, -10);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("ABZCGIiji4IAAC0IgpAAIAAkHIAqAAICUCnIAAinIApAAIAAELg");
        this.shape_18.setTransform(-200.4, -8.1);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_18
            }, {
                t: this.shape_17
            }, {
                t: this.shape_16
            }, {
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-217.3, -34, 434.6, 68.1);


    (lib.Tween11 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgJBaIgJgGQgDgDgDgEQgCgGAAgFQAAgGACgFQADgEADgEIAJgFQAFgDAEAAQAGAAAEADIAJAFQAEAEACAEQACAFAAAGQAAAFgCAGQgCAEgEADIgJAGQgEACgGAAQgEAAgFgCgAgJgoIgJgGQgDgEgDgEQgCgFAAgGQAAgGACgEIAGgJIAJgGQAFgBAEAAQAGAAAEABIAJAGQAEAEACAFQACAEAAAGQAAAGgCAFQgCAEgEAEIgJAGQgEACgGAAQgEAAgFgCg");
        this.shape.setTransform(146.1, -4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_1.setTransform(128.7, -5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IAqgWIARAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFAAgGQAAgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAFAFACAFQADAFgBAHQABAGgDAFQgCAGgFADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_2.setTransform(109.9, -10);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AgNCBQgJgDgHgGQgGgHgDgJQgDgJAAgLIAAiAIggAAIAAgPIAggKIAAgpIApgWIASAAIAAA/IAxAAIAAAZIgxAAIAAB/QAAAFAEAFQACAFAGgBIAHgCIAGgFIAHgGIADgEIAVAQIgLAPQgGAHgIAEQgHAFgLADQgKADgNAAQgLAAgKgEg");
        this.shape_3.setTransform(95, -8);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AgkBfQgTgIgOgNQgNgOgHgSQgIgSAAgUQAAgOADgNQAFgOAHgMQAGgMAJgKQAKgKANgHQALgHAOgEQANgDANAAQAPAAAMACQALABAKAFQAIAEAIAHIAOARIgjAaIgEgJQgCgGgFgHQgFgGgIgFQgJgFgNAAQgIAAgJAFQgHAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQAEAPAGAMQAIAMAJAGQAIAHANAAQAJAAAJgEQAIgEAGgHQAHgGAGgKIAKgUIAcAIQgFAUgJAPQgJAPgMAKQgNALgPAGQgQAFgSAAQgUAAgRgIg");
        this.shape_4.setTransform(74.2, -5.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_5.setTransform(49.9, -5);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_6.setTransform(31.3, -10.2);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_7.setTransform(18.6, -10.2);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AgrBfQgUgIgPgNQgOgOgJgSQgIgTAAgVQAAgWAIgTQAJgUAOgOQAPgOAUgHQAUgIAXAAQAXAAAVAHQAUAIAPANQAOAOAJASQAIASAAAVQAAAVgIAUQgIATgPAPQgPAOgUAIQgUAJgYAAQgXAAgUgIgAgVhJQgHAFgFAJQgFAJgDALQgDAMAAANQAAASAEARQADARAHANQAGANAKAHQAKAIALAAQAIAAAHgGQAHgFAFgJQAFgJADgMQADgMAAgNQAAgSgDgRQgDgQgGgNQgGgNgLgHQgKgIgOAAQgHAAgGAGg");
        this.shape_8.setTransform(-0.8, -5.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AglBfQgSgIgOgNQgNgOgIgSQgHgSAAgUQAAgOAEgNQADgOAIgMQAGgMAKgKQAJgKAMgHQAMgHAOgEQAMgDAOAAQAQAAALACQAMABAIAFQAJAEAHAHIAPARIgjAaIgEgJQgCgGgFgHQgFgGgJgFQgIgFgMAAQgKAAgHAFQgIAEgGAJQgFAIgDALQgDALAAALQAAARAEAOQADAPAIAMQAGAMAKAGQAJAHAMAAQAKAAAIgEQAHgEAHgHQAHgGAGgKIAKgUIAcAIQgFAUgJAPQgJAPgNAKQgMALgQAGQgPAFgSAAQgUAAgSgIg");
        this.shape_9.setTransform(-25.9, -5.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AgfBjQgNgEgJgHQgJgGgGgJIgKgQIAigOQAFAQAKAKQAKAJAPAAIAKgBIAJgFIAGgGQACgFAAgDQAAgIgCgFQgDgGgFgEIgLgJIgNgGIgXgMQgMgGgJgHQgKgIgFgKQgGgKAAgOQAAgMAFgKQAFgKAKgIQAJgHAOgFQANgDAQAAQAOgBALADQALADAIAFQAJAFAHAHQAHAIAEAIIgjANQgCgKgEgFIgIgIIgIgDIgIAAIgJABIgJADIgGAGQgDAEAAAEQAAAGADAFQADAEAFAFIAMAIIAPAIIAZALQAMAGAJAIQAJAIAGAKQAGAJgBAMQABAOgFALQgEALgKAJQgJAHgPAFQgQAGgVAAQgSgBgNgEg");
        this.shape_10.setTransform(-57.1, -5);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AgFA6QgBgLgEgLIgIgVIgHgVQgDgKAAgMQAAgGACgFQADgFADgEIAKgHQAFgCAFAAQAGAAAFACQAFADAFAEQADAEADAFQACAFAAAGQAAAMgDAKIgHAVIgIAVQgDALgBALg");
        this.shape_11.setTransform(-71.1, -15.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("AhABkQgMgEgHgHQgIgHgEgKQgEgJAAgLQAAgRAJgMQAKgNAQgJQAPgIAUgHIAogNIAAgTQAAgKgCgGQgCgHgEgEQgDgEgEgCIgLgCQgHAAgGACQgGADgEAFIgGALIgEAMIgzAAQADgMAIgKQAIgLALgHQAMgHAPgEQAPgEARAAQAUAAAQAEQAQAEAJAHQALAHAEAKQAFAKAAANIAABxQAAAMAIAJQAHAIAOAGIhHAAIgHgCIgFgFIgEgGIgCgGIgLAJIgJAHIgNAFIgSACQgOAAgKgEgAgGAGQgJAFgHAHQgHAGgFAIQgEAIAAAJQAAAKAFAGQAFAFAJAAQAHAAAHgEIAQgMIAAg4g");
        this.shape_12.setTransform(-86.1, -5.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_13.setTransform(-104, -10.2);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFFFFF").s().p("AgdCWIAAkVIApgWIASAAIAAErg");
        this.shape_14.setTransform(-116.7, -10.2);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_15.setTransform(-135.2, -5);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#FFFFFF").s().p("Ag5CPQgUgDgOgGQgOgGgGgIQgHgIAAgJQAAgHAEgHIAKgNIAPgNIAQgNQgMgEgHgGQgHgFAAgIQAAgFADgGIAIgMIAOgJIAPgJIgSgKQgHgFgGgHQgFgHgDgIQgDgJAAgLQgBgRAIgOQAHgNAOgKQANgKARgFQASgGAVAAIATACIASAEIAOAFIALAHIA9AAIAAAVIglAAIAIAQQADAIAAAKQABARgIANQgIANgNAIQgOAKgSAEQgTAFgWAAIgOgBIgMgCQgEADgDAEQgCAGAAAFIABADIAEACIAJACIAQACIAZACIAlACQATAAAOAEQAOAEAHAGQAJAGAEAHQADAJAAAJQAAAQgJAPQgIAOgSAKQgQAKgYAGQgYAGgdAAQgbAAgVgEgAg1A+IgFAHIgDAIIAAAIQAAAGAEAGQAEAFAJAFQAIAEANADQAMADARAAQARAAAPgDQAPgEAKgFQAKgEAFgGQAGgGgBgGQABgLgMgFQgLgGgTAAIgbgBIgZgBIgWgCIgQgBgAgWh6QgGAEgEAHQgDAGgCAJIgCASQAAAMADALQACAKAGAIQAEAIAHAFQAIAFAIAAQAGAAAGgEQAGgDAFgHQAFgGADgIQADgIAAgJQAAgNgEgLQgDgLgGgIQgFgJgIgFQgHgFgIAAQgJAAgFAEg");
        this.shape_16.setTransform(-159.3, -0.8);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AgdCVIAAi0IApgWIASAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFgBgGQABgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQACAFAAAHQAAAGgCAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_17.setTransform(-178.3, -10);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("ABZCGIiji4IAAC0IgpAAIAAkHIAqAAICUCnIAAinIApAAIAAELg");
        this.shape_18.setTransform(-200.4, -8.1);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_18
            }, {
                t: this.shape_17
            }, {
                t: this.shape_16
            }, {
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-217.3, -34, 434.6, 68.1);


    (lib.NigellaBoek1 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

    }).prototype = getMCSymbolPrototype(lib.NigellaBoek1, null, null);


    (lib._2NigellaBoek5 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigella5();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._2NigellaBoek5, new cjs.Rectangle(0, 0, 550, 733), null);


    (lib._2NigellaBoek4 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigella4();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._2NigellaBoek4, new cjs.Rectangle(0, 0, 550, 710), null);


    (lib._2NigellaBoek3 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigella3();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._2NigellaBoek3, new cjs.Rectangle(0, 0, 550, 633), null);


    (lib._2NigellaBoek2 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigella2();
        this.instance.parent = this;
        this.instance.setTransform(0, 0, 0.469, 0.488);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib._2NigellaBoek2, new cjs.Rectangle(0, 0, 257.8, 348.1), null);


    (lib.Tween3 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FF9900").s().p("AgJCEIgJgGQgEgDgCgFQgCgFAAgFQAAgGACgFQACgFAEgDIAJgGQAEgCAFAAQAGAAAEACIAJAGIAGAIQACAFAAAGQAAAFgCAFIgGAIIgJAGQgEACgGAAQgFAAgEgCgAgJA9IgGgsIgEgdIgFgXIgDgRIgDgRIgBgUQAAgLADgIQADgIAEgGQAEgFAHgDQAFgDAFAAQAGAAAFADQAHADAEAFQAEAGADAIQADAIAAALIgBAUIgDARIgDARIgEAXIgFAdIgGAsg");
        this.shape.setTransform(-8.9, -8.2);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FF9900").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_1.setTransform(-28.4, -5);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FF9900").s().p("AAfBmIAAiHIgCgOIgDgKIgHgGIgIgCIgJABIgJAFIgLAHIgMAJIAACRIg8AAIAAi0IAqgXIASAAIAAAVQAOgLAQgEQAPgFAQAAQAMAAALAFQALAFAJAHQAHAJAFAKQAGAMAAAMIAACOg");
        this.shape_2.setTransform(-54.2, -5.3);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FF9900").s().p("AgdCVIAAi0IAqgWIASAAIAADKgAgMhbIgKgHQgEgDgDgGQgDgFABgGQgBgHADgFQADgFAEgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQADAFgBAHQABAGgDAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_3.setTransform(-74.2, -10);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FF9900").s().p("AgfBjQgNgEgJgHQgJgGgGgJIgKgQIAigOQAFAQAKAKQAKAJAPAAIAKgBIAJgFIAGgGQACgFAAgDQAAgIgCgFQgDgGgFgEIgLgJIgNgGIgXgMQgMgGgJgHQgKgIgFgKQgGgKAAgOQAAgMAFgKQAFgKAKgIQAJgHAOgFQANgDAQAAQAOgBALADQALADAIAFQAJAFAHAHQAHAIAEAIIgjANQgCgKgEgFIgIgIIgIgDIgIAAIgJABIgJADIgGAGQgDAEAAAEQAAAGADAFQADAEAFAFIAMAIIAPAIIAZALQAMAGAJAIQAJAIAGAKQAGAJgBAMQABAOgFALQgEALgKAJQgJAHgPAFQgQAGgVAAQgSgBgNgEg");
        this.shape_4.setTransform(-91.8, -5);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FF9900").s().p("AgdCVIAAi0IApgWIASAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFgBgGQABgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAEAFADAFQACAFAAAHQAAAGgCAFQgDAGgEADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_5.setTransform(-109.1, -10);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FF9900").s().p("AgvBhQgNgGgKgIQgJgJgFgLQgGgMAAgNIAAiKIA9AAIAACKQAAAHABAFQADAFAEAEQAEADAFACQAGACAFAAIAHgBIAJgFIAJgIIAHgJIAAiPIA8AAIAADGIg8AAIAAgTQgMAKgNAGQgNAHgMgBQgPAAgNgEg");
        this.shape_6.setTransform(-129.4, -4.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FF9900").s().p("AgsCAQgdgKgVgSQgUgSgMgaQgMgZAAgfQAAgdALgZQALgaAVgSQAUgSAcgLQAcgKAhAAQAXAAATAFQASAFAQAKQAPAKAMAMQANANAJAQIgbATIgSgWQgKgLgLgHQgLgIgNgEQgMgFgNAAQgSAAgQAHQgQAGgNANQgMAOgHAUQgIATAAAZQAAAZAIAVQAIATANANQAMANARAIQAQAGASAAQAOAAAMgEQAMgFAKgJQAKgHAKgLIARgXIAbAUQgJAOgMANQgMANgQAKQgPAKgSAGQgSAGgUAAQghAAgdgKg");
        this.shape_7.setTransform(-159.7, -8.3);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FF9900").s().p("AgmBgQgTgJgOgMQgOgOgIgSQgIgRAAgUQAAgWAIgVQAHgTAOgPQANgPATgIQATgJAWABQAVAAARAGQASAHAMALQAMAMAHAPQAHAPAAASIiJAAQABARAEAOQAEAPAHALQAHALAKAIQAJAGALAAQAKAAAJgEQAKgDAIgGQAIgFAGgIQAGgJADgIIAfAEQgFAQgKAPQgKAOgOAKQgOALgQAGQgQAHgRAAQgUAAgSgIgAAagnQgBgIgDgIQgDgGgFgGQgEgFgHgDQgFgCgHAAQgGAAgFACQgFADgFAFQgEAFgDAHQgDAIgBAIIBDAAIAAAAg");
        this.shape_8.setTransform(-200, -5);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FF9900").s().p("AAfCWIAAiIQAAgOgFgIQgFgIgIAAIgJABIgKAFIgMAHIgMAHIAACSIg9AAIAAkVIArgWIASAAIAAB1QAOgKAQgFQARgFARAAQAMAAAKAFQALAFAIAIQAIAIAEALQAFALAAAMIAACOg");
        this.shape_9.setTransform(-225.9, -10.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FF9900").s().p("AgNCBQgJgDgHgGQgGgHgDgJQgDgJAAgLIAAiAIggAAIAAgPIAggKIAAgpIApgWIASAAIAAA/IAxAAIAAAZIgxAAIAAB/QAAAFAEAFQACAFAGgBIAGgCIAHgFIAHgGIADgEIAVAQIgLAPQgGAHgIAEQgHAFgKADQgLADgNAAQgLAAgKgEg");
        this.shape_10.setTransform(-247.7, -8);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FF9900").s().p("AgiDAIgQgGIgLgJIgJgIIAggYQAJAOAJAGQAJAFAKABIADgCIAEgFIACgJIAAgNIAAjbIArgWIASAAIAADSQAAAbgHAQQgHAQgLAKQgLAJgQACQgPAEgQAAQgLAAgJgDgAAZiJQgFgDgFgEQgEgDgDgGQgCgFAAgGQAAgHACgFQADgFAEgFQAFgDAFgCQAGgDAHAAQAHAAAGADQAGACAEADQAEAFADAFQADAFAAAHQAAAGgDAFQgDAGgEADQgEAEgGADQgGACgHAAQgHAAgGgCg");
        this.shape_11.setTransform(-278, -5.4);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FF9900").s().p("AgdCVIAAi0IApgWIASAAIAADKgAgMhbIgKgHQgFgDgCgGQgCgFgBgGQABgHACgFQACgFAFgFIAKgFQAHgDAFAAQAHAAAGADQAGACAEADQAFAFACAFQADAFgBAHQABAGgDAFQgCAGgFADQgEAEgGADQgGACgHAAQgFAAgHgCg");
        this.shape_12.setTransform(-288.3, -10);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FF9900").s().p("Ag8CSQgVgHgSgNIAAj/IArgWIASAAIAABzQAMgIANgEQANgFANAAQATAAAQAHQAQAHALAMQAMAMAGAPQAHAQAAASQAAAQgFAPQgEAPgHANQgIANgKALQgLALgNAIQgNAHgPAFQgOAEgPAAQgYAAgVgGgAgWgKQgJAFgHAHIAABuIAHAFIAHAFIAIADIAJABQAJAAAJgGQAHgGAGgLQAHgLACgOQADgPABgRQgBgMgDgMQgDgLgGgIQgFgIgIgFQgIgEgHAAQgJAAgJAEg");
        this.shape_13.setTransform(-307.8, -10);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FF9900").s().p("AgvBhQgNgGgKgIQgJgJgFgLQgGgMAAgNIAAiKIA9AAIAACKQAAAHACAFQACAFAEAEQAEADAFACQAGACAGAAIAGgBIAJgFIAJgIIAHgJIAAiPIA8AAIAADGIg8AAIAAgTQgMAKgNAGQgNAHgMgBQgPAAgNgEg");
        this.shape_14.setTransform(-345.5, -4.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FF9900").s().p("ABZCGIiji4IAAC0IgpAAIAAkHIApAAICVCnIAAinIApAAIAAELg");
        this.shape_15.setTransform(-374.6, -8.1);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-391.5, -34, 783.2, 68.1);


    (lib.Tween1 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nieuwsherojamieoliver();
        this.instance.parent = this;
        this.instance.setTransform(-290.5, -163.4, 0.303, 0.303);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-290.5, -163.4, 581.2, 326.9);


    (lib.onsaanbodtext3 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgmB7QgOgHgKgMQgLgMgFgQQgHgPAAgSQAAgRAIgQQAHgPAMgNQANgNARgHQASgIASAAIAOABIAMADIAAhFIAkgTIAPAAIAAEBIgzAAIAAgKQgKAGgLAEQgLAEgJAAQgRAAgOgIgAgIgVQgGAFgFAIQgEAIgDAKQgEALAAANQABAOADANQADANAGAKQAFAKAHAFQAHAGAIAAIAGgBIAHgCIAGgEIAFgFIAAhSQAAgIgDgGQgCgHgEgFQgEgFgGgDQgFgCgGAAQgFAAgHAEg");
        this.shape.setTransform(303, 20.9);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AglBSQgRgHgNgMQgMgLgHgQQgHgQAAgSQAAgTAHgQQAHgRAMgMQANgMARgHQARgGAUAAQAUAAARAGQASAHAMALQANAMAHAPQAHAQAAASQAAASgHAQQgHARgMANQgNAMgRAHQgSAHgUAAQgTAAgSgGgAgSg+QgGAEgEAIQgFAHgCAKQgDAKAAALQAAAQADAOQADAPAGALQAGALAIAGQAIAHAKAAQAHAAAGgFQAGgFAEgHQAFgIACgKQADgKAAgLQAAgQgDgOQgCgPgGgKQgFgLgJgHQgJgGgLAAQgHAAgFAFg");
        this.shape_1.setTransform(281.4, 25.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AgzB9QgTgFgOgMIAAjaIAkgTIAQAAIAABiQAKgGAMgEQAKgEALAAQAQAAAOAGQANAGAKAKQAKALAGAMQAFAOAAAQQAAANgDANQgEANgGALQgHALgJAKQgJAJgLAHQgLAGgMAEQgNADgNAAQgUAAgSgFgAgTgIQgIAEgFAGIAABeIAFAFIAGAEIAHACIAIABQAIAAAHgFQAHgFAFgJQAFgKADgMQADgNAAgOQAAgLgDgKQgDgJgFgIQgFgGgHgEQgGgEgHAAQgHAAgIAEg");
        this.shape_2.setTransform(259.9, 20.9);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AAbBXIAAhzIgBgMIgEgIIgGgGIgGgBIgIABIgIADIgKAGIgJAIIAAB8Ig0AAIAAiaIAkgTIAQAAIAAARQAMgJANgEQANgDAOAAQAKAAAJADQAKAFAHAGQAHAIAEAJQAEAJAAALIAAB5g");
        this.shape_3.setTransform(237.1, 24.9);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("Ag3BWQgJgEgHgGQgHgGgDgIQgEgIAAgJQAAgPAJgKQAIgLANgIQAOgHARgFIAhgMIAAgQQAAgIgBgGQgCgGgDgEQgDgDgDgCIgKgBQgGAAgFACQgFACgDAEIgGAKIgDAKIgrAAQACgKAHgJQAHgJAKgGQAJgGANgDQANgEAPAAQARAAANAEQAOADAIAGQAJAGAEAJQAEAIAAALIAABhQAAAKAHAIQAGAHAMAFIg9AAIgGgCIgEgEIgEgFIgCgFIgJAIIgIAGIgLAEIgPABQgMAAgJgDgAgFAFQgIAFgGAFQgGAGgEAHQgEAHAAAHQAAAJAFAFQAEAEAIAAQAGAAAGgDIANgKIAAgwg");
        this.shape_4.setTransform(217, 25);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("Ag3BWQgJgEgHgGQgHgGgDgIQgEgIAAgJQAAgPAJgKQAIgLANgIQAOgHARgFIAhgMIAAgQQAAgIgBgGQgCgGgDgEQgDgDgDgCIgKgBQgGAAgFACQgFACgDAEIgGAKIgDAKIgrAAQACgKAHgJQAHgJAKgGQAJgGANgDQANgEAPAAQARAAANAEQAOADAIAGQAJAGAEAJQAEAIAAALIAABhQAAAKAHAIQAGAHAMAFIg9AAIgGgCIgEgEIgEgFIgCgFIgJAIIgIAGIgLAEIgPABQgMAAgJgDgAgFAFQgIAFgGAFQgGAGgEAHQgEAHAAAHQAAAJAFAFQAEAEAIAAQAGAAAGgDIANgKIAAgwg");
        this.shape_5.setTransform(197.8, 25);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AgbBVQgLgEgIgFQgHgGgFgHIgJgPIAdgLQAFAOAIAIQAJAIANAAIAJgCIAHgEIAFgFQACgDAAgEQAAgGgDgFQgCgEgDgEIgKgHIgLgGIgUgKQgKgFgIgGQgIgHgFgJQgFgIAAgMQAAgKAEgJQAFgJAIgHQAIgGAMgDQALgEAOAAQALAAAKACQAJADAIAEQAHAEAGAHQAGAGAEAIIgeAKQgCgIgDgFQgEgEgDgCIgIgDIgGgBIgIABIgIAEIgFAEQgCADAAAEQAAAFACAEQADAFAFAEIAKAHIAMAGIAWAKQAKAFAIAGQAHAHAFAJQAFAIAAAKQAAAMgEAJQgDAKgJAHQgHAHgNAEQgOAFgSAAQgPAAgMgEg");
        this.shape_6.setTransform(171.3, 25.1);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AAbBXIAAhzIgBgMIgEgIIgGgGIgGgBIgIABIgIADIgKAGIgJAIIAAB8Ig0AAIAAiaIAkgTIAQAAIAAARQAMgJANgEQANgDAOAAQAKAAAJADQAKAFAHAGQAHAIAEAJQAEAJAAALIAAB5g");
        this.shape_7.setTransform(151.9, 24.9);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AglBSQgRgHgNgMQgMgLgHgQQgHgQAAgSQAAgTAHgQQAHgRAMgMQANgMARgHQARgGAUAAQAUAAARAGQASAHAMALQANAMAHAPQAHAQAAASQAAASgHAQQgHARgMANQgNAMgRAHQgSAHgUAAQgTAAgSgGgAgSg+QgGAEgEAIQgFAHgCAKQgDAKAAALQAAAQADAOQADAPAGALQAGALAIAGQAIAHAKAAQAHAAAGgFQAGgFAEgHQAFgIACgKQADgKAAgLQAAgQgDgOQgCgPgGgKQgFgLgJgHQgJgGgLAAQgHAAgFAFg");
        this.shape_8.setTransform(129.9, 25.1);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AAZCBIg6hFIAABFIg0AAIAAjuIAkgTIAQAAIAACRIBCg6IAlAAIhJBAIBZBqg");
        this.shape_9.setTransform(101.9, 20.7);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AgdClIgNgGIgKgHIgIgHIAcgUQAHAMAIAFQAIAEAIABIADgCIADgEIACgIIAAgMIAAi7IAlgTIAPAAIAAC1QAAAWgGAOQgFAPgKAHQgKAIgNADQgNACgNAAQgKAAgIgCgAAWh2QgFgCgEgDQgEgDgCgFQgCgFAAgEQAAgGACgEQACgFAEgDQAEgEAFgCQAFgCAGAAQAFAAAFACQAGACADAEQAEADACAFQADAEAAAGQAAAEgDAFQgCAFgEADQgDADgGACQgFACgFABQgGgBgFgCg");
        this.shape_10.setTransform(81.6, 24.8);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AgZCAIAAiaIAkgTIAPAAIAACtgAgKhOIgJgGIgGgIQgCgEAAgFQAAgGACgEIAGgIIAJgGQAFgCAFAAQAGAAAFACQAFACAEAEQAEADACAFQACAEAAAGQAAAFgCAEQgCAFgEADQgEAEgFACQgFACgGAAQgFAAgFgCg");
        this.shape_11.setTransform(73.5, 20.8);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("AAZCBIg6hFIAABFIg0AAIAAjuIAkgTIAQAAIAACRIBCg6IAlAAIhJBAIBZBqg");
        this.shape_12.setTransform(59.1, 20.7);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_13.setTransform(37.4, 25.1);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFFFFF").s().p("AhhBxIAAjhIBhAAQASAAAQAEQARAEALAIQAMAIAGAKQAHALAAANQAAAIgCAHQgCAGgEAGQgEAHgGAFIgQALQAKADAIAFQAIAFAGAHQAGAHAEAJQADAIAAAKQAAANgGAMQgGALgLAJQgLAIgQAFQgQAFgTAAgAgqBaIAfAAQAMAAAKgDQAKgDAGgFQAHgFADgHQADgHAAgJQAAgJgDgIQgDgIgGgHQgHgFgKgEQgKgDgOAAIgdAAgAgqgOIAlAAQAHAAAHgDQAHgCAFgGQAFgFACgGQADgHAAgIQAAgJgDgHQgEgHgGgGQgHgFgKgDQgJgDgNAAIgVAAg");
        this.shape_14.setTransform(14.7, 22.3);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = getMCSymbolPrototype(lib.onsaanbodtext3, new cjs.Rectangle(0, 0, 332.5, 58.9), null);


    (lib.nigellajamietext2 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgIBxIgHgEQgDgDgCgFQgCgEAAgFIACgJIAFgHIAHgFQAEgCAEAAQAFAAAEACIAHAFIAFAHQACAFAAAEQAAAFgCAEIgFAIIgHAEQgEACgFAAQgEAAgEgCgAgHA0IgFglIgFgZIgEgUIgDgPIgCgOIAAgRQAAgJACgHQACgHAEgFQAEgFAFgDQAFgCAEAAQAFAAAFACQAFADAEAFQAEAFACAHQACAHAAAJIAAARIgDAOIgCAPIgEAUIgFAZIgEAlg");
        this.shape.setTransform(862.2, 22.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AAbBXIAAhzIgBgMIgEgIIgGgGIgGgBIgIABIgIADIgKAGIgJAIIAAB8Ig0AAIAAiaIAkgTIAQAAIAAARQAMgJANgEQANgDAOAAQAKAAAJADQAKAFAHAGQAHAIAEAJQAEAJAAALIAAB5g");
        this.shape_1.setTransform(845.9, 24.9);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_2.setTransform(824.6, 25.1);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#FFFFFF").s().p("AAZCBIg6hFIAABFIg0AAIAAjuIAkgTIAQAAIAACRIBCg6IAlAAIhJBAIBZBqg");
        this.shape_3.setTransform(805.3, 20.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_4.setTransform(783.6, 25.1);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#FFFFFF").s().p("AglBSQgRgHgNgMQgMgLgHgQQgHgQAAgSQAAgTAHgQQAHgRAMgMQANgMARgHQARgGAUAAQAUAAARAGQASAHAMALQANAMAHAPQAHAQAAASQAAASgHAQQgHARgMANQgNAMgRAHQgSAHgUAAQgTAAgSgGgAgSg+QgGAEgEAIQgFAHgCAKQgDAKAAALQAAAQADAOQADAPAGALQAGALAIAGQAIAHAKAAQAHAAAGgFQAGgFAEgHQAFgIACgKQADgKAAgLQAAgQgDgOQgCgPgGgKQgFgLgJgHQgJgGgLAAQgHAAgFAFg");
        this.shape_5.setTransform(761.7, 25.1);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#FFFFFF").s().p("AgzB9QgTgFgOgMIAAjaIAkgTIAQAAIAABiQAKgGAMgEQAKgEALAAQAQAAAOAGQANAGAKAKQAKALAGAMQAFAOAAAQQAAANgDANQgEANgGALQgHALgJAKQgJAJgLAHQgLAGgMAEQgNADgNAAQgUAAgSgFgAgTgIQgIAEgFAGIAABeIAFAFIAGAEIAHACIAIABQAIAAAHgFQAHgFAFgJQAFgKADgMQADgNAAgOQAAgLgDgKQgDgJgFgIQgFgGgHgEQgGgEgHAAQgHAAgIAEg");
        this.shape_6.setTransform(740.2, 20.9);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_7.setTransform(709.7, 25.1);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#FFFFFF").s().p("AgLBvQgIgDgFgGQgFgFgDgIQgDgHAAgKIAAhuIgcAAIAAgMIAcgJIAAgjIAjgTIAQAAIAAA2IAqAAIAAAVIgqAAIAABtQAAAFACAEQADAEAEAAIAHgCIAFgFIAFgEIAEgEIARAOIgJAMQgFAGgHAEQgHAEgIADQgJACgLAAQgKAAgIgDg");
        this.shape_8.setTransform(691.9, 22.6);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#FFFFFF").s().p("AgbBVQgLgEgIgFQgHgGgFgHIgJgPIAdgLQAFAOAJAIQAIAIANAAIAIgCIAIgEIAFgFQACgDAAgEQAAgGgDgFQgCgEgDgEIgKgHIgLgGIgUgKQgKgFgIgGQgIgHgFgJQgFgIAAgMQAAgKAFgJQAEgJAIgHQAIgGALgDQAMgEAOAAQALAAAKACQAKADAHAEQAHAEAGAHQAGAGAEAIIgeAKQgDgIgCgFQgEgEgEgCIgHgDIgGgBIgIABIgIAEIgFAEQgCADAAAEQAAAFACAEQADAFAEAEIALAHIAMAGIAVAKQAKAFAIAGQAJAHAEAJQAFAIAAAKQAAAMgDAJQgEAKgJAHQgHAHgNAEQgOAFgSAAQgQAAgLgEg");
        this.shape_9.setTransform(675.8, 25.1);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#FFFFFF").s().p("AAnBYIglhVIgkBVIgZAAIhOitIA2AAIAtBiIArhkIALAAIAtBjIArhhIAiAAIhLCtg");
        this.shape_10.setTransform(652.6, 25.2);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#FFFFFF").s().p("AgoBTQgLgFgJgHQgHgHgFgKQgFgKAAgLIAAh3IA0AAIAAB3QAAAFACAFQACAEADADQAEAEAEACQAFABAFAAIAFgBIAIgFIAHgGIAGgJIAAh6IA0AAIAACqIg0AAIAAgQQgKAJgLAFQgLAFgLAAQgNAAgKgEg");
        this.shape_11.setTransform(627, 25.2);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_12.setTransform(605.6, 25.1);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#FFFFFF").s().p("AgZCAIAAiaIAkgTIAPAAIAACtgAgKhOIgJgGIgGgIQgCgEAAgFQAAgGACgEIAGgIIAJgGQAFgCAFAAQAGAAAFACQAFACAEAEQAEADACAFQACAEAAAGQAAAFgCAEQgCAFgEADQgEAEgFACQgFACgGAAQgFAAgFgCg");
        this.shape_13.setTransform(589.4, 20.8);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#FFFFFF").s().p("AAbBXIAAhzIgBgMIgEgIIgGgGIgGgBIgIABIgIADIgKAGIgJAIIAAB8Ig0AAIAAiaIAkgTIAQAAIAAARQAMgJANgEQANgDAOAAQAKAAAJADQAKAFAHAGQAHAIAEAJQAEAJAAALIAAB5g");
        this.shape_14.setTransform(572.8, 24.9);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#FFFFFF").s().p("AgaBVQgMgEgHgFQgIgGgFgHIgJgPIAegLQAEAOAIAIQAJAIANAAIAJgCIAHgEIAFgFQACgDAAgEQAAgGgCgFQgCgEgFgEIgJgHIgLgGIgUgKQgKgFgIgGQgIgHgFgJQgFgIAAgMQAAgKAEgJQAFgJAIgHQAJgGALgDQALgEAOAAQALAAAKACQAJADAIAEQAHAEAGAHQAFAGAFAIIgeAKQgDgIgCgFQgEgEgDgCIgIgDIgGgBIgIABIgIAEIgFAEQgCADAAAEQAAAFADAEQACAFAFAEIAKAHIAMAGIAWAKQAJAFAJAGQAHAHAFAJQAFAIAAAKQAAAMgEAJQgDAKgIAHQgJAHgMAEQgOAFgSAAQgPAAgLgEg");
        this.shape_15.setTransform(545.5, 25.1);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#FFFFFF").s().p("AgEAyIgFgTIgGgSIgGgRQgDgKAAgKQAAgFACgEQACgEADgEIAIgFQAFgDAEAAQAFAAAFADQAEACAEADQAEAEABAEQACAEAAAFQAAAKgCAKIgGARIgHASQgEAKAAAJg");
        this.shape_16.setTransform(533.3, 15.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#FFFFFF").s().p("AAbBXIAAhzIgBgMIgEgIIgGgGIgGgBIgIABIgIADIgKAGIgJAIIAAB8Ig0AAIAAiaIAkgTIAQAAIAAARQAMgJANgEQANgDAOAAQAKAAAJADQAKAFAHAGQAHAIAEAJQAEAJAAALIAAB5g");
        this.shape_17.setTransform(518.8, 24.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#FFFFFF").s().p("AglBSQgRgHgNgMQgMgLgHgQQgHgQAAgSQAAgTAHgQQAHgRAMgMQANgMARgHQARgGAUAAQAUAAARAGQASAHAMALQANAMAHAPQAHAQAAASQAAASgHAQQgHARgMANQgNAMgRAHQgSAHgUAAQgTAAgSgGgAgSg+QgGAEgEAIQgFAHgCAKQgDAKAAALQAAAQADAOQADAPAGALQAGALAIAGQAIAHAKAAQAHAAAGgFQAGgFAEgHQAFgIACgKQADgKAAgLQAAgQgDgOQgCgPgGgKQgFgLgJgHQgJgGgLAAQgHAAgFAFg");
        this.shape_18.setTransform(496.8, 25.1);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#FFFFFF").s().p("AgbBVQgLgEgIgFQgHgGgFgHIgJgPIAdgLQAFAOAJAIQAIAIANAAIAIgCIAIgEIAFgFQACgDAAgEQAAgGgDgFQgCgEgDgEIgKgHIgLgGIgUgKQgKgFgIgGQgIgHgFgJQgFgIAAgMQAAgKAFgJQAEgJAIgHQAIgGALgDQAMgEAOAAQALAAAKACQAKADAHAEQAHAEAGAHQAGAGAEAIIgeAKQgCgIgDgFQgEgEgEgCIgHgDIgGgBIgIABIgIAEIgFAEQgCADAAAEQAAAFACAEQADAFAEAEIALAHIAMAGIAVAKQAKAFAIAGQAJAHAEAJQAFAIAAAKQAAAMgDAJQgEAKgJAHQgHAHgNAEQgOAFgSAAQgQAAgLgEg");
        this.shape_19.setTransform(476.9, 25.1);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#FFFFFF").s().p("AAnBYIglhVIgkBVIgZAAIhOitIA2AAIAtBiIArhkIALAAIAtBjIArhhIAiAAIhLCtg");
        this.shape_20.setTransform(453.7, 25.2);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#FFFFFF").s().p("Ag3BWQgJgEgHgGQgHgGgDgIQgEgIAAgJQAAgPAJgKQAIgLANgIQAOgHARgFIAhgMIAAgQQAAgIgBgGQgCgGgDgEQgDgDgDgCIgKgBQgGAAgFACQgFACgDAEIgGAKIgDAKIgrAAQACgKAHgJQAHgJAKgGQAJgGANgDQANgEAPAAQARAAANAEQAOADAIAGQAJAGAEAJQAEAIAAALIAABhQAAAKAHAIQAGAHAMAFIg9AAIgGgCIgEgEIgEgFIgCgFIgJAIIgIAGIgLAEIgPABQgMAAgJgDgAgFAFQgIAFgGAFQgGAGgEAHQgEAHAAAHQAAAJAFAFQAEAEAIAAQAGAAAGgDIANgKIAAgwg");
        this.shape_21.setTransform(429.7, 25);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#FFFFFF").s().p("AhUBxIAAjhIA3AAIAADKIByAAIAAAXg");
        this.shape_22.setTransform(410, 22.3);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#FFFFFF").s().p("Ag3BWQgJgEgHgGQgHgGgDgIQgEgIAAgJQAAgPAJgKQAIgLANgIQAOgHARgFIAhgMIAAgQQAAgIgBgGQgCgGgDgEQgDgDgDgCIgKgBQgGAAgFACQgFACgDAEIgGAKIgDAKIgrAAQACgKAHgJQAHgJAKgGQAJgGANgDQANgEAPAAQARAAANAEQAOADAIAGQAJAGAEAJQAEAIAAALIAABhQAAAKAHAIQAGAHAMAFIg9AAIgGgCIgEgEIgEgFIgCgFIgJAIIgIAGIgLAEIgPABQgMAAgJgDgAgFAFQgIAFgGAFQgGAGgEAHQgEAHAAAHQAAAJAFAFQAEAEAIAAQAGAAAGgDIANgKIAAgwg");
        this.shape_23.setTransform(380.8, 25);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#FFFFFF").s().p("AgZCBIAAjuIAjgTIAQAAIAAEBg");
        this.shape_24.setTransform(365.3, 20.7);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f("#FFFFFF").s().p("AgZCBIAAjuIAjgTIAQAAIAAEBg");
        this.shape_25.setTransform(354.3, 20.7);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_26.setTransform(338.3, 25.1);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AgwB7QgSgDgMgFQgMgFgFgHQgGgHAAgIQAAgGADgFIAJgMIAMgLIAOgLQgKgDgGgFQgGgFAAgHQAAgEACgFIAIgKIALgIIANgIIgPgIQgGgFgFgGQgFgGgCgHQgDgHAAgJQAAgPAGgMQAHgMALgIQAMgJAOgEQAQgFASAAIAQACIAPADIANAFIAJAFIA0AAIAAASIggAAIAIAOQACAHAAAJQAAAOgHALQgGALgMAIQgLAHgQAEQgRAEgSAAIgMAAIgKgCQgEADgCADQgCAFAAAEIABADIAEACIAHABIAOACIAVACIAgABQAQABAMADQALADAHAFQAIAFADAHQADAHAAAIQAAAOgHAMQgJAMgOAJQgOAJgVAFQgUAFgZAAQgYAAgQgDgAguA1IgDAHIgDAGIgBAHQAAAFAEAFQADAFAIADQAHAEALADQALACAOAAQAPAAANgCQAMgDAIgFQAJgEAFgFQAEgFAAgEQABgKgLgFQgJgFgRAAIgXAAIgVgBIgTgCIgNgBgAgThoQgFADgDAGQgEAFgBAIIgCAQQAAAKADAJQACAJAEAHQAFAHAGAEQAGAEAHAAQAFAAAFgDQAFgDAEgGQAFgFACgHQADgHAAgIQAAgLgDgJQgDgKgFgHQgFgHgGgEQgGgFgHAAQgHAAgFAEg");
        this.shape_27.setTransform(317.5, 28.8);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.f("#FFFFFF").s().p("AgZCAIAAiaIAkgTIAPAAIAACtgAgKhOIgJgGIgGgIQgCgEAAgFQAAgGACgEIAGgIIAJgGQAFgCAFAAQAGAAAFACQAFACAEAEQAEADACAFQACAEAAAGQAAAFgCAEQgCAFgEADQgEAEgFACQgFACgGAAQgFAAgFgCg");
        this.shape_28.setTransform(301.1, 20.8);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.f("#FFFFFF").s().p("ABMBzIiMieIAACaIgjAAIAAjhIAkAAICACPIAAiPIAiAAIAADlg");
        this.shape_29.setTransform(282.1, 22.5);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.f("#FFFFFF").s().p("AAnBuQgNgFgNgJQgNAJgRAFQgRAFgVAAQgRAAgOgEQgNgEgJgGQgJgHgFgJQgFgKAAgLQAAgMAFgKQAFgKAIgIQAIgJALgHIAWgNQgFgNgDgMQgDgMAAgMQAAgLAFgLQAFgLAJgIQAIgJANgFQAMgFAPAAQAKAAAKADQAKACAJAGQAIAFAFAJQAFAJAAANQAAALgFAJQgGAJgJAJQgIAIgMAHIgWANQAHAPAJAOQAIAOALAMIAOgPIAMgRIALgQIAHgNIAaAQIgYAjQgMARgNAOQAIAGAJAEQAIAEAJAAQAHAAAFgDQAGgCAEgEIAHgJIAGgMIAUAJQgIAZgPALQgQALgYAAQgNAAgNgFgAhIAmQgHAJAAALQAAAGACAGQACAFAEAEQAEAFAGACQAGADAJAAQAJAAAJgDQAJgDAJgGQgNgMgLgOQgLgOgJgPQgLAIgHAIgAgShaQgEACgDAFQgDAFgCAGQgCAGAAAIIABAOIAEAPIANgJIALgKIAHgLQACgGAAgHIgBgKIgEgGIgEgEIgHgBQgEAAgEADg");
        this.shape_30.setTransform(247.2, 22.4);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.f("#FFFFFF").s().p("Ag/BYIAAiaIAkgTIAQARQAIgJAKgFQAKgGALABQAMgBAJAFQAJAEAGAGIgSAbIgGgEIgGgEIgGgCIgHgBIgKACIgKAFIgHAHIgFAJIAAB6g");
        this.shape_31.setTransform(218.1, 24.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_32.setTransform(198.1, 25.1);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.f("#FFFFFF").s().p("AgMBXIhSitIA2AAIAxBnIAyhnIAkAAIhUCtg");
        this.shape_33.setTransform(177.2, 25.3);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.f("#FFFFFF").s().p("AgZCAIAAiaIAkgTIAPAAIAACtgAgKhOIgJgGIgGgIQgCgEAAgFQAAgGACgEIAGgIIAJgGQAFgCAFAAQAGAAAFACQAFACAEAEQAEADACAFQACAEAAAGQAAAFgCAEQgCAFgEADQgEAEgFACQgFACgGAAQgFAAgFgCg");
        this.shape_34.setTransform(161.2, 20.8);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.f("#FFFFFF").s().p("AgZCBIAAjuIAjgTIAQAAIAAEBg");
        this.shape_35.setTransform(149.9, 20.7);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.f("#FFFFFF").s().p("AgyBtQgYgJgRgQQgSgQgJgWQgKgWAAgZQAAgZAKgWQAJgVASgQQARgPAYgIQAXgJAbAAQAbAAAYAJQAXAIASAQQARAPAKAVQAKAVAAAaQAAARgEAQQgFAQgIANQgJANgLALQgMAKgOAIQgPAHgQAEQgRAEgSAAQgbAAgXgJgAgdhbQgMAHgJANQgIANgFASQgEASAAAVQAAAVAEASQAFATAIANQAJAOAMAHQANAIAQAAQAQAAANgIQANgHAIgOQAJgNAEgTQAEgSAAgVQAAgVgEgSQgEgSgJgNQgIgNgNgHQgNgHgQAAQgQAAgNAHg");
        this.shape_36.setTransform(128.8, 22.3);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.f("#FFFFFF").s().p("AghBSQgQgHgMgLQgLgMgHgPQgHgPAAgRQAAgTAGgRQAGgRAMgNQAMgNAQgHQAQgHATAAQASAAAPAGQAPAFAKAKQALAKAGANQAGAOAAAPIh2AAQABAOAEAMQADANAGAJQAGAKAIAGQAIAGAJAAQAJAAAIgDQAIgDAHgFQAHgFAFgHQAGgHACgIIAaAFQgEANgIANQgJAMgMAJQgMAJgOAFQgNAGgPAAQgRAAgQgHgAAWghQAAgHgDgHQgDgGgEgEQgEgFgFgCQgEgDgGAAQgFAAgFADQgFACgDAFQgEAEgDAGQgCAHgBAHIA5AAIAAAAg");
        this.shape_37.setTransform(94.7, 25.1);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.f("#FFFFFF").s().p("AgZCAIAAiaIAkgTIAPAAIAACtgAgKhOIgJgGIgGgIQgCgEAAgFQAAgGACgEIAGgIIAJgGQAFgCAFAAQAGAAAFACQAFACAEAEQAEADACAFQACAEAAAGQAAAFgCAEQgCAFgEADQgEAEgFACQgFACgGAAQgFAAgFgCg");
        this.shape_38.setTransform(78.5, 20.8);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.f("#FFFFFF").s().p("ABPBXIAAhzIgBgKIgEgJIgGgGQgEgDgEABIgJABIgJAEIgJAGIgIAHIAAB8IgxAAIAAhzIgCgKIgEgJIgGgGQgDgDgEABIgJABIgKAEIgJAGIgHAHIAAB8Ig0AAIAAiaIAkgTIAQAAIAAATQALgJANgFQAOgFAMABIAOABIAMAEIALAHIAJAJIANgJIAOgHIAQgEIARgBQALAAAJADQAKAEAHAIQAHAHAFAKQAEAKAAAMIAAB2g");
        this.shape_39.setTransform(56.2, 24.9);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.f("#FFFFFF").s().p("Ag3BWQgJgEgHgGQgHgGgDgIQgEgIAAgJQAAgPAJgKQAIgLANgIQAOgHARgFIAhgMIAAgQQAAgIgBgGQgCgGgDgEQgDgDgDgCIgKgBQgGAAgFACQgFACgDAEIgGAKIgDAKIgrAAQACgKAHgJQAHgJAKgGQAJgGANgDQANgEAPAAQARAAANAEQAOADAIAGQAJAGAEAJQAEAIAAALIAABhQAAAKAHAIQAGAHAMAFIg9AAIgGgCIgEgEIgEgFIgCgFIgJAIIgIAGIgLAEIgPABQgMAAgJgDgAgFAFQgIAFgGAFQgGAGgEAHQgEAHAAAHQAAAJAFAFQAEAEAIAAQAGAAAGgDIANgKIAAgwg");
        this.shape_40.setTransform(30.2, 25);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.f("#FFFFFF").s().p("AgbBxIgSgGIgPgHIgOgIIAQgUIAIAHIALAGIANAFQAHACAHAAQAPAAAIgIQAJgIAAgNIAAixIA3AAIAAClQAAAOgFAMQgGALgJAJQgLAIgPAFQgPAFgTAAQgMAAgKgCg");
        this.shape_41.setTransform(9.5, 22.5);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_41
            }, {
                t: this.shape_40
            }, {
                t: this.shape_39
            }, {
                t: this.shape_38
            }, {
                t: this.shape_37
            }, {
                t: this.shape_36
            }, {
                t: this.shape_35
            }, {
                t: this.shape_34
            }, {
                t: this.shape_33
            }, {
                t: this.shape_32
            }, {
                t: this.shape_31
            }, {
                t: this.shape_30
            }, {
                t: this.shape_29
            }, {
                t: this.shape_28
            }, {
                t: this.shape_27
            }, {
                t: this.shape_26
            }, {
                t: this.shape_25
            }, {
                t: this.shape_24
            }, {
                t: this.shape_23
            }, {
                t: this.shape_22
            }, {
                t: this.shape_21
            }, {
                t: this.shape_20
            }, {
                t: this.shape_19
            }, {
                t: this.shape_18
            }, {
                t: this.shape_17
            }, {
                t: this.shape_16
            }, {
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = getMCSymbolPrototype(lib.nigellajamietext2, new cjs.Rectangle(0, 0, 879.3, 58.9), null);


    (lib.nigellalawsonhero = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.nigellalawson();
        this.instance.parent = this;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib.nigellalawsonhero, new cjs.Rectangle(0, 0, 3005, 2000), null);


    (lib.logocuisine = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("Ai8ChQgTgBgLAFQBfiMAfgxQA1hRAAgbQAAgLgFAAQgIAAgfAhQgfAhgtA6Qg2BJg6AzQhEA7gxAAQgLAAgHgJQgIgJAAgOQABgYAggyQASgdA2hEIBZh2QADAOASgEQASgEAQgSQgWAigkAxIg8BSIhMBlQghAtABAKQAAABAAAAQAAABAAAAQABABAAAAQABAAAAAAQAtAABBg4QA6gxAxhFIACAAQBih/AbgHIAJgBQAPAAAJAKQAJALAAATQAAAXgRAeQA3hFAigRQARgGAJAAQAUAAALAMQAMAMAAAUQAAAsgvA+IhNBkQggAtAAAKQAAABAAABQAAAAABABQAAAAABAAQAAAAABAAQAXAAA5g0QA4g1AyhEQAqg7A0gmQA4gpA0AAQAxAAgUAsQgSAshBAhQguAXg0AGQgiAsgVAtQgXArAAAUQABALAHAAQAsAABSg6QBPg4Avg9IAAAAIALAAQgvBBhTA7QhWA9gwAAQgSAAgJgWQgGgPgBgRQAAgfATgrQgtA1goAiQgtAmgUAAQgMAAgHgJQgIgJABgOQAAgZAbgsQATggAtg4QAZghAPgbQAQgcgBgPQABgPgQAAQgGAAgMAFQgpAXhWB3QgtBAhGBmQADgHgVgCgAFCh4QgfAZglAwQgKAIgQAZQArgGArgXQAvgYARgpQARgmgXAAQgTAAgfAag");
        this.shape.setTransform(176.8, 128.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgNAfQgJgBgDgKQgDgJAFgNQAHgNAKgIQAKgIAJABQALABADAJQACAKgGANQgGANgLAIQgIAHgIAAIgDAAg");
        this.shape_1.setTransform(161.1, 103.7);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f("#000000").s().p("AlnFvQikhVhsieQB5CDCaBFQCiBICsgGQBrgFBmggQBnghBZg7QBag9BThrQgvAdg/AAQhFAAhzhPQiJhcgugPQgVA4guAgQgpAegxAFQgvAFgggTQgigUAAgjQAAgkAvgYQAvgYBHAAQAcAAAfAFQANgtALhSQAPhtAEgXQgRAbguA6Qg0BKg6AyQhEA8gxAAQgLAAgHgJQgIgJAAgOQAAgVAZgpQgqAqgkAaQgmAbgTAAQgLAAgHgJQgIgJAAgOQAAgVAbgrQgrArglAaQgmAbgTAAQgLAAgHgJQgHgJAAgOQAAgZAggxQAUgfA0hDIBZh2QADAMAQgDQARgDANgPIAIAAQgkAzhTBuIhMBmQggAsAAALQAAAAAAABQAAABAAAAQABAAAAABQABAAABAAQAWAAA4guQA3guAwg6IAigtIBZh2QADAMAQgDQARgDANgPIAIAAQgkAzhTBuIhMBmQggAsAAALQAAAAAAABQAAABAAAAQABAAAAABQABAAABAAQAYAAA5gwQA4gvAvg7IAfgpIBZh2QADAOATgEQARgEAPgSQgUAhglAyIg8BSIhMBmQggAsAAALQAAAAAAABQAAABAAAAQABAAAAABQABAAABAAQAtAABBg4QA5gxAxhGQA8hMAagqQAFgKAGgcQAEgUAKgEIAHgBQAHAAAEAGQAFAFgBAJQgBAIgPAOQgTAQgFALIgBADIgDANQgEAWAEA/QAGBSAAAiQAAAngLApQAwAMCFBMQBxBDBFAAQBvAAAyg1IARgYQAAgDACgCIAAACIAFAAQgCAGgGANQhVC5ipB2QitB4jHAIIgeAAQitAAiahPgAh9g/QgoAWgEAfQgCAWAKAOQANAUAfAAQAvAAAnghQAmghAUg8QgsgFgKAAQg6AAgoAWg");
        this.shape_2.setTransform(124.2, 153);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.f("#000000").s().p("AgNAfQgJgBgDgKQgDgKAGgMQAFgNALgIQAKgIAJABQALABADAJQACAKgGANQgGANgLAIQgIAHgIAAIgDAAg");
        this.shape_3.setTransform(125.8, 103.7);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.f("#000000").s().p("Ak8E3QglgkAAhDQAAhaA+hqQgegOgQgXQgQgXAAgaQAAgyAzglQA4gqBbAAQAgAAAXAEQBLg6BdgrQBGgcA0gLQAcgGAiAAQAwAAAbATQAbATAAAhQAAAmgiAwQgkAvg/AwQhMA7hfAhQhsAlh2AAQgpAAgbgHQg2BvAABZQgBA0AYAcQAXAbAsAAQBmAABBhQQA3hEgEhFQgCgygsAOQguAPgOA3IgIAAQAFglAggZQAdgWAgAAQA0AAAIArQAIApgiA3QghA5g7AmQhEArhMAAQhGAAgmgjgAjxAUQAjAHAfAAQBhAABmgrQBZglBPg/QBihOAbg/QAJgTAAgOQAAgshCAAQhEAAhUAiQhfAkhSBIQA/APAwAeQAxAfAaApIgFAFQgbgqgygeQgygehAgNQhnBeg7BvgAkniWQgxAlABAtQAAAXAOATQAOAUAbAKQA/hpBvhZIgngCQhXAAg3Aqg");
        this.shape_4.setTransform(68.6, 114.7);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.f("#000000").s().p("AGwgkQiHiMi4g8Qi6g8i9AmQjKApijCRQCPilDRgzQDDgwDGA9QDHA9CICWQCSCfAdDYQg2jJiOiSg");
        this.shape_5.setTransform(130.6, 76);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.f("#000000").s().p("AgJA5QgPgFgJgGIgRgSQgJgOgCgFIASgKQACAFAHALQAEAGAKAIQAKAHAFAAQAFAAADgEQADgDgCgEIgEgIIgWgbQgEgHgCgJQgCgIAIgKQAGgIAJgDQAKgDAJACQAOAEAKAHQAGAEAKAKQAGAGAEAKIgSALQgHgPgNgKQgJgGgGAAQgGAAgDAFQgDAFADAFQACAFAJAKIAOAQQAEAIACAIQACAJgIAJQgHAIgIADQgHACgEAAIgIgBg");
        this.shape_6.setTransform(171.1, 48.6);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.f("#000000").s().p("AgCA9IgMg4IgVAHIgOAZIgXgMIA0hiIAWAMIgWAtIBDgWIAaAMIhAAYIAPBMg");
        this.shape_7.setTransform(161.7, 42.6);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.f("#000000").s().p("AgRA3QgSgGgJgLQgLgMgDgPQgEgPAGgPQAHgQAKgKQALgKAQgCQAOgEAQAGQASAGAJAMQALANADAOQAEAPgGAOQgHASgKAIQgLAKgQAEIgJABQgKAAgLgFgAgGghQgHACgJAGQgGAGgEAKQgDALABAHQACAJAGAHQAGAIALACQAJACAIgBQAGAAAKgIQAGgGAEgJQADgMgBgHQgBgHgHgIQgGgIgKgCQgHgCgFAAIgGAAg");
        this.shape_8.setTransform(148.4, 36.7);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.f("#000000").s().p("AgJA5QgRgEgMgIQgOgLgEgNQgGgSADgMQADgQAJgMQALgNAOgEQAQgFAPADQAQADAOAJQAMAJAGAPQAGAPgDAPQgDAQgKAMQgMANgNAEQgKADgKAAIgLgBgAgMggQgKADgEAHQgGAIgBAKQgCALAEAHQACAGAIAJQAEAFANACQAIACAJgEQAKgDAEgHQAGgKABgIQABgLgDgHQgFgKgFgGQgGgEgLgDIgFAAQgHAAgFADg");
        this.shape_9.setTransform(134.1, 33.2);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.f("#000000").s().p("AAHA4Ig6gBIAChvIA2ABQAVAAALAIQALAIgBANQAAAJgDAFQgHAIgFACQAHADAHAIQAFAHABALQAAAPgNAHQgLAIgQAAIgFgBgAgYAmIAcAAQAJgBAHgDQAEgCABgIQAAgOgVAAIgcAAgAgYgkIAAAaIAZAAQAJAAAGgDQAFgDAAgHQAAgHgFgDQgDgDgMgBg");
        this.shape_10.setTransform(120.5, 32.1);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.f("#000000").s().p("Ag7gvIAZgEIAHAzIApg6IAbgEIgmA4IA5A1IgdAEIgogmIgNASIAEAcIgZAEg");
        this.shape_11.setTransform(107.6, 33.1);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.f("#000000").s().p("AgPA5QgQgDgLgKQgLgKgFgPQgFgRAEgNQADgQAMgKQAOgMAOgFQARgFAOADQAQADALAKQALAJAGARQAFARgEAMQgEARgLAJQgPANgOAEQgIADgKAAIgNgBgAgKggQgKACgGAIQgHAIgBAHQgCAHADAMQACAKAIAFQAHAHAIABQAJACAJgDQALgEAEgGQAGgHADgIQACgJgDgKQgDgKgHgFQgIgHgIgBIgHgBQgFAAgFACg");
        this.shape_12.setTransform(93.6, 36.2);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.f("#000000").s().p("AghAzQgNgIgIgPQgHgPABgOQABgRAJgLQAJgNAQgIQAQgIAOAAQAOAAAPAIQANAIAIAPQAIAPgCAOQgCARgIALQgMAOgMAHQgOAIgRAAQgPAAgOgIgAgQggQgJAFgEAHQgGAIAAAJQAAAIAFAKQAEAJAJAFQAHAFAKABQAIABAJgFQAJgDAFgJQAFgJAAgIQAAgJgEgJQgGgLgIgDQgGgFgKgBQgJAAgJAEg");
        this.shape_13.setTransform(79.9, 41.9);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.f("#000000").s().p("AgYA3QgOgGgKgOQgJgLgCgSQgCgPAIgNQAJgRALgHQALgIAOgDQALgDANADIgFAWQgOgFgPALQgJAGgDAJQgFAJACAIQAAAIAGAKQAGAJAJADQAJAEAJgBQAJgCAJgFQANgLACgQIAWADQgCALgFANQgFAJgNALQgYAJgHABIgFABQgOAAgKgFg");
        this.shape_14.setTransform(69.1, 48.4);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.f("#000000").s().p("AgLAKQgEgFABgGQACgGAEgEQAEgEAGABQAHAAADAGQAFAFgCAGQgCAHgEADQgEADgGAAQgIgCgCgEg");
        this.shape_15.setTransform(176.8, 194);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.f("#000000").s().p("AgMAJQgDgEAAgHQACgFAFgFQAGgDAFAAQAGABAEAGQADAEAAAGQgCAGgFAFQgEADgHAAQgGgCgEgFg");
        this.shape_16.setTransform(172.1, 197.3);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.f("#000000").s().p("AgCAPQgHgCgDgFQgEgGACgFQACgGAEgDQAHgDAFABQAFACAFAFQADAHgBAEQgCAHgGACQgEACgEAAIgCAAg");
        this.shape_17.setTransform(167.3, 200.4);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.f("#000000").s().p("Ag+ggIBMgkIAJATIg1AZIAKAWIAvgWIAJASIgvAVIALAXIA2gZIAJATIhOAlg");
        this.shape_18.setTransform(157.8, 200.8);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.f("#000000").s().p("Ag+gsIAugOQANgEAMABQANACAIAHQAJAIACALQAEAPgEAHQgCAKgJAHIAhAbIgZAIIgegYIgBAAIgVAHIAJAeIgZAIgAgLgmIgUAGIALAkIAUgFQAKgDAFgHQAEgFgCgLQgCgIgJgDQgEgBgDAAIgKABg");
        this.shape_19.setTransform(146.1, 205.8);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.f("#000000").s().p("AgXA3QgOgGgLgMQgJgMgDgRQgCgOAGgPQAFgMAOgLQAMgJASgDQAQgDAPAGQAOAFALANQAJAMADAQQABAQgFANQgFANgOALQgNAJgRADIgIAAQgLAAgMgDgAgCgiQgKACgHAFQgFADgGAKQgDALABAHQACAMAFAGQAHAJAHACQALAEAGgCQAKgBAHgGQAFgDAGgKQADgLgBgHQgCgNgFgFQgHgJgHgCQgHgDgGAAIgEABg");
        this.shape_20.setTransform(132.2, 208.2);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.f("#000000").s().p("AAmA4IABhCIgjA1IgKAAIggg2IgCBBIgYgBIAFhvIAUACIAnBGIArhDIAWABIgFBvg");
        this.shape_21.setTransform(116.7, 209.1);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.f("#000000").s().p("AgNA7IgxgOIAfhrIAwAOQARAEAMAMQALAJAFAQQAEANgFAQQgFASgLAIQgKAKgQADIgMACQgJAAgLgEgAggAgIAXAGQAPAEANgGQANgGADgQQAFgOgIgMQgHgMgQgEIgWgGg");
        this.shape_22.setTransform(95.6, 205.2);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.f("#000000").s().p("AACBAIgVhUIgcA9IgWgNIAwhkIATAJIAUBUIAdg9IAXANIgwBkg");
        this.shape_23.setTransform(82.6, 200.7);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.f("#000000").s().p("AASA1IAHgZIgrgcIgVAMIgXgNIBohCIAUAPIgUB3gAAAgLIAcASIAJgsg");
        this.shape_24.setTransform(68.6, 195.2);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.f().s("#000000").ss(3, 1).p("AAAwZQDVAADDBSQC9BQCSCRQCRCSBQC9QBSDDAADUQAADVhSDEQhQC8iRCSQiSCRi9BQQjDBSjVAAQjUAAjDhSQi9hQiSiRQiRiShQi8QhSjEAAjVQAAjUBSjDQBQi9CRiSQCSiRC9hQQDDhSDUAAg");
        this.shape_25.setTransform(120.6, 120.6);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.f("#FFFFFF").s().p("AmXPIQi9hQiRiRQiSiShPi8QhTjDAAjWQAAjVBTjCQBPi9CSiRQCRiSC9hPQDChTDVAAQDVAADEBTQC8BPCSCSQCRCRBQC9QBSDCAADVQAADWhSDDQhQC8iRCSQiSCRi8BQQjEBSjVAAQjVAAjChSg");
        this.shape_26.setTransform(120.6, 120.6);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.f("#FFFFFF").s().p("AnSRRQjYhbimilQiminhajXQhfjfAAj0QAAjzBfjfQBajYCmimQCmimDYhaQDfhfDzAAQD0AADfBfQDXBaCnCmQClCmBbDYQBfDfAADzQAAD0hfDfQhbDXilCnQinCljXBbQjfBfj0AAQjzAAjfhfg");
        this.shape_27.setTransform(120.6, 120.6);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_27
            }, {
                t: this.shape_26
            }, {
                t: this.shape_25
            }, {
                t: this.shape_24
            }, {
                t: this.shape_23
            }, {
                t: this.shape_22
            }, {
                t: this.shape_21
            }, {
                t: this.shape_20
            }, {
                t: this.shape_19
            }, {
                t: this.shape_18
            }, {
                t: this.shape_17
            }, {
                t: this.shape_16
            }, {
                t: this.shape_15
            }, {
                t: this.shape_14
            }, {
                t: this.shape_13
            }, {
                t: this.shape_12
            }, {
                t: this.shape_11
            }, {
                t: this.shape_10
            }, {
                t: this.shape_9
            }, {
                t: this.shape_8
            }, {
                t: this.shape_7
            }, {
                t: this.shape_6
            }, {
                t: this.shape_5
            }, {
                t: this.shape_4
            }, {
                t: this.shape_3
            }, {
                t: this.shape_2
            }, {
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).wait(1));

    }).prototype = getMCSymbolPrototype(lib.logocuisine, new cjs.Rectangle(0.7, 0.7, 239.9, 239.9), null);


    (lib.playbutton = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FF9900").s().p("AjnkNIHREPInTEMg");
        this.shape.setTransform(68.6, 44.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AlFFFQiGiHAAi+QAAi+CGiHQCHiGC+AAQC+AACHCGQCCCDAFC0InRkPIgCIbIHTkMIAAAOQAAC+iHCHQiHCHi+AAQi+AAiHiHg");
        this.shape_1.setTransform(46, 46);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.lf(["#000000", "#FFFFFF"], [0, 1], -23.4, 0, 23.4, 0).s().p("AjnkNIHREPInTEMg");
        this.shape_2.setTransform(68.6, 44.4);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.lf(["#000000", "#FFFFFF"], [0, 1], -46, 0, 46, 0).s().p("AlFFFQiGiHAAi+QAAi+CGiHQCHiGC+AAQC+AACHCGQCCCDAFC0InRkPIgCIbIHTkMIAAAOQAAC+iHCHQiHCHi+AAQi+AAiHiHg");
        this.shape_3.setTransform(46, 46);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.lf(["#FFFFFF", "#FF9900"], [0, 1], -23.4, 0, 23.4, 0).s().p("AjnkNIHREPInTEMg");
        this.shape_4.setTransform(68.6, 44.4);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.lf(["#FFFFFF", "#FF9900"], [0, 1], -46, 0, 46, 0).s().p("AlFFFQiGiHAAi+QAAi+CGiHQCHiGC+AAQC+AACHCGQCCCDAFC0InRkPIgCIbIHTkMIAAAOQAAC+iHCHQiHCHi+AAQi+AAiHiHg");
        this.shape_5.setTransform(46, 46);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }).to({
            state: [{
                t: this.shape_3
            }, {
                t: this.shape_2
            }]
        }, 1).to({
            state: [{
                t: this.shape_1
            }, {
                t: this.shape
            }]
        }, 1).to({
            state: [{
                t: this.shape_5
            }, {
                t: this.shape_4
            }]
        }, 1).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0, 0, 92, 92);


    (lib.bijdecuisinetext1 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.Tween3("synched", 0);
        this.instance.parent = this;
        this.instance.setTransform(391.6, 402.6);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib.bijdecuisinetext1, new cjs.Rectangle(0, 368.6, 783.2, 68.1), null);


    (lib.jamieoliver = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer_1
        this.instance = new lib.Tween1("synched", 0);
        this.instance.parent = this;
        this.instance.setTransform(290.6, -237.5);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = getMCSymbolPrototype(lib.jamieoliver, new cjs.Rectangle(0, -401, 581.2, 326.9), null);


    // stage content:
    (lib.nigellabanner = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_129 = function() {
            /* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/

            this.stop();

            /* Click to Go to Frame and Play
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and continues playback from that frame.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/

            this.bekijkButton.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));

            function fl_ClickToGoToAndPlayFromFrame() {
                this.gotoAndPlay(180);
            }
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(726));

        // 1Hero-jamie
        this.instance = new lib.jamieoliver();
        this.instance.parent = this;
        this.instance.setTransform(339.7, 204.5, 1, 1, 0, 0, 0, 290.6, 163.5);

        this.timeline.addTween(cjs.Tween.get(this.instance).to({
            y: 617.1
        }, 29).wait(150).to({
            alpha: 0
        }, 35).to({
            _off: true
        }, 7).wait(634));

        // 1Text-bij-cuisine
        this.instance_1 = new lib.bijdecuisinetext1();
        this.instance_1.parent = this;
        this.instance_1.setTransform(440.7, 444, 1, 1, 0, 0, 0, 391.6, 34);

        this.timeline.addTween(cjs.Tween.get(this.instance_1).to({
            y: 103.5
        }, 49).wait(130).to({
            alpha: 0
        }, 35).to({
            _off: true
        }, 7).wait(634));

        // 1Text-jamie-nigella
        this.heroText2_mc = new lib.nigellajamietext2();
        this.heroText2_mc.name = "heroText2_mc";
        this.heroText2_mc.parent = this;
        this.heroText2_mc.setTransform(488.8, 899.4, 1, 1, 0, 0, 0, 439.7, 29.4);

        this.timeline.addTween(cjs.Tween.get(this.heroText2_mc).to({
            y: 554.4
        }, 59).wait(120).to({
            alpha: 0
        }, 35).to({
            _off: true
        }, 7).wait(634));

        // 1Text-bekijk-aanbod
        this.instance_2 = new lib.onsaanbodtext3();
        this.instance_2.parent = this;
        this.instance_2.setTransform(215.3, 989.4, 1, 1, 0, 0, 0, 166.2, 29.4);

        this.timeline.addTween(cjs.Tween.get(this.instance_2).to({
            y: 649.4
        }, 69).wait(110).to({
            alpha: 0
        }, 35).to({
            _off: true
        }, 7).wait(634));

        // 1Play-button
        this.instance_3 = new lib.playbutton();
        this.instance_3.parent = this;
        this.instance_3.setTransform(381.6, 935);
        new cjs.ButtonHelper(this.instance_3, 0, 1, 2, false, new lib.playbutton(), 3);

        this.bekijkButton = new lib.playbutton();
        this.bekijkButton.name = "bekijkButton";
        this.bekijkButton.parent = this;
        this.bekijkButton.setTransform(381.6, 600);
        this.bekijkButton._off = true;
        new cjs.ButtonHelper(this.bekijkButton, 0, 1, 2, false, new lib.playbutton(), 3);

        this.timeline.addTween(cjs.Tween.get(this.instance_3).to({
            _off: true,
            y: 600
        }, 69).wait(786));
        this.timeline.addTween(cjs.Tween.get(this.bekijkButton).to({
            _off: false
        }, 69).wait(110).to({
            alpha: 0
        }, 35).to({
            _off: true
        }, 7).wait(634));

        // 1Hero-nigella
        this.nigellaLawsonHero_mc = new lib.nigellalawsonhero();
        this.nigellaLawsonHero_mc.name = "nigellaLawsonHero_mc";
        this.nigellaLawsonHero_mc.parent = this;
        this.nigellaLawsonHero_mc.setTransform(1569.5, 1006.7, 0.2, 0.2, 0, 0, 0, 1502.7, 999.8);

        this.timeline.addTween(cjs.Tween.get(this.nigellaLawsonHero_mc).wait(39).to({
            y: 478.8
        }, 45).wait(95).to({
            alpha: 0
        }, 35).to({
            _off: true
        }, 7).wait(634));

        // 1Cuisine-logo
        this.instance_4 = new lib.logocuisine();
        this.instance_4.parent = this;
        this.instance_4.setTransform(951.8, 259, 1, 1, 0, 0, 0, 120.2, 120);
        this.instance_4.alpha = 0;

        this.instance_5 = new lib.NigellaBoek1();
        this.instance_5.parent = this;
        this.instance_5.setTransform(952.3, 259.6, 1, 1, 0, 0, 0, 120, 120);

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.instance_4
            }]
        }).to({
            state: [{
                t: this.instance_4
            }]
        }, 83).to({
            state: [{
                t: this.instance_4
            }]
        }, 46).to({
            state: [{
                t: this.instance_4
            }]
        }, 50).to({
            state: [{
                t: this.instance_4
            }]
        }, 34).to({
            state: [{
                t: this.instance_5
            }]
        }, 1).to({
            state: []
        }, 7).wait(634));
        this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(83).to({
            alpha: 1
        }, 46).wait(50).to({
            x: 952.3,
            y: 259.5,
            alpha: 0
        }, 34).to({
            _off: true,
            regX: 120,
            y: 259.6,
            alpha: 1
        }, 1).wait(641));

        // 2Text-Nigella
        this.instance_6 = new lib.Tween11("synched", 0);
        this.instance_6.parent = this;
        this.instance_6.setTransform(-271.8, 104.9);
        this.instance_6._off = true;

        this.instance_7 = new lib.Tween12("synched", 0);
        this.instance_7.parent = this;
        this.instance_7.setTransform(256.3, 105.6);
        this.instance_7._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(215).to({
            _off: false
        }, 0).to({
            _off: true,
            x: 256.3,
            y: 105.6
        }, 32).wait(608));
        this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(215).to({
            _off: false
        }, 32).wait(222).to({
            startPosition: 0
        }, 0).to({
            x: 2168.8
        }, 55).to({
            _off: true
        }, 11).wait(320));

        // 2Nigella-Boek1
        this.instance_8 = new lib.Tween13("synched", 0);
        this.instance_8.parent = this;
        this.instance_8.setTransform(554, -215);
        this.instance_8._off = true;

        this.instance_9 = new lib.Tween14("synched", 0);
        this.instance_9.parent = this;
        this.instance_9.setTransform(554, 206);
        this.instance_9._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(251).to({
            _off: false
        }, 0).to({
            _off: true,
            y: 206
        }, 74).wait(530));
        this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(251).to({
            _off: false
        }, 74).wait(94).to({
            startPosition: 0
        }, 0).to({
            y: 952.7
        }, 60).to({
            _off: true
        }, 56).wait(320));

        // 2Nigella-Boek2
        this.instance_10 = new lib._2NigellaBoek2();
        this.instance_10.parent = this;
        this.instance_10.setTransform(1074.1, -214.9, 1.009, 1.005, 0, 0, 0, 128.9, 174.1);
        this.instance_10._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(251).to({
            _off: false
        }, 0).to({
            y: 204.6
        }, 74).wait(94).to({
            y: 952.8
        }, 60).to({
            _off: true
        }, 56).wait(320));

        // 2Nigella-Boek3
        this.instance_11 = new lib._2NigellaBoek3();
        this.instance_11.parent = this;
        this.instance_11.setTransform(1594.1, -214.8, 0.473, 0.553, 0, 0, 0, 275.2, 316.9);
        this.instance_11._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(251).to({
            _off: false
        }, 0).to({
            y: 204.7
        }, 74).wait(94).to({
            y: 952.9
        }, 60).to({
            _off: true
        }, 56).wait(320));

        // 2Nigella-Boek4
        this.instance_12 = new lib._2NigellaBoek5();
        this.instance_12.parent = this;
        this.instance_12.setTransform(814.1, 985.1, 0.473, 0.477, 0, 0, 0, 275.1, 366.6);
        this.instance_12._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(251).to({
            _off: false
        }, 0).to({
            y: 555.1
        }, 74).wait(94).to({
            y: -201.7
        }, 60).to({
            _off: true
        }, 56).wait(320));

        // 2Nigella-Boek5
        this.instance_13 = new lib._2NigellaBoek4();
        this.instance_13.parent = this;
        this.instance_13.setTransform(1334.1, 985, 0.473, 0.493, 0, 0, 0, 275.1, 354.9);
        this.instance_13._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(251).to({
            _off: false
        }, 0).to({
            y: 554.5
        }, 74).wait(94).to({
            y: -201.7
        }, 60).to({
            _off: true
        }, 56).wait(320));

        // 3Jamie-Text
        this.instance_14 = new lib.Tween15("synched", 0);
        this.instance_14.parent = this;
        this.instance_14.setTransform(-173, 105.6);
        this.instance_14._off = true;

        this.instance_15 = new lib.Tween16("synched", 0);
        this.instance_15.parent = this;
        this.instance_15.setTransform(243.1, 103.2);
        this.instance_15._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(534).to({
            _off: false
        }, 0).to({
            _off: true,
            x: 243.1,
            y: 103.2
        }, 35).wait(286));
        this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(534).to({
            _off: false
        }, 35).wait(150).to({
            startPosition: 0
        }, 0).wait(55).to({
            startPosition: 0
        }, 0).to({
            x: 2159.5
        }, 65).wait(16));

        // 3Jamie-Boek1
        this.instance_16 = new lib._3JamieBoek1();
        this.instance_16.parent = this;
        this.instance_16.setTransform(789.2, -215, 0.473, 0.493, 0, 0, 0, 275.1, 355);
        this.instance_16._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(569).to({
            _off: false
        }, 0).to({
            y: 198.2
        }, 60).wait(90).to({
            y: 965
        }, 60).wait(76));

        // 3Jamie-Boek2
        this.instance_17 = new lib._3JamieBoek2();
        this.instance_17.parent = this;
        this.instance_17.setTransform(1309.2, -215, 0.473, 0.485, 0, 0, 0, 275, 360.5);
        this.instance_17._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(569).to({
            _off: false
        }, 0).to({
            y: 208.1
        }, 60).wait(90).to({
            y: 965
        }, 60).wait(76));

        // 3Jamie-Boek3
        this.instance_18 = new lib._3JamieBoek3();
        this.instance_18.parent = this;
        this.instance_18.setTransform(529.2, 975, 0.473, 0.493, 0, 0, 0, 275.1, 355);
        this.instance_18._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(569).to({
            _off: false
        }, 0).to({
            y: 548.2
        }, 60).wait(90).to({
            y: -205
        }, 60).wait(76));

        // 3Jamie-Boek4
        this.instance_19 = new lib._3JamieBoek4();
        this.instance_19.parent = this;
        this.instance_19.setTransform(1049.2, 975.1, 0.473, 0.491, 0, 0, 0, 275.1, 356.6);
        this.instance_19._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(569).to({
            _off: false
        }, 0).to({
            y: 548.3
        }, 60).wait(90).to({
            y: -204.9
        }, 60).wait(76));

        // 3Jamie-Boek5
        this.instance_20 = new lib._3JamieBoek5();
        this.instance_20.parent = this;
        this.instance_20.setTransform(1569.2, 975, 0.473, 0.485, 0, 0, 0, 275.1, 361);
        this.instance_20._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(569).to({
            _off: false
        }, 0).to({
            y: 558.1
        }, 60).wait(90).to({
            y: -205
        }, 60).wait(76));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(1009.1, 15, 1821, 1566.8);
    // library properties:
    lib.properties = {
        id: '5F627695D751418490F36BAA3DC98329',
        width: 1920,
        height: 750,
        fps: 30,
        color: "#000000",
        opacity: 1.00,
        manifest: [{
            src: "../animate/images/jamie1.jpg",
            id: "jamie1"
        }, {
            src: "../animate/images/jamie2.jpg",
            id: "jamie2"
        }, {
            src: "../animate/images/jamie3.jpg",
            id: "jamie3"
        }, {
            src: "../animate/images/jamie4.jpg",
            id: "jamie4"
        }, {
            src: "../animate/images/jamie5.jpg",
            id: "jamie5"
        }, {
            src: "../animate/images/nieuwsherojamieoliver.jpg",
            id: "nieuwsherojamieoliver"
        }, {
            src: "../animate/images/nigella1.jpg",
            id: "nigella1"
        }, {
            src: "../animate/images/nigella2.jpg",
            id: "nigella2"
        }, {
            src: "../animate/images/nigella3.jpg",
            id: "nigella3"
        }, {
            src: "../animate/images/nigella4.jpg",
            id: "nigella4"
        }, {
            src: "../animate/images/nigella5.jpg",
            id: "nigella5"
        }, {
            src: "../animate/images/nigellalawson.jpg",
            id: "nigellalawson"
        }],
        preloads: []
    };

    // bootstrap callback support:

    (lib.Stage = function(canvas) {
        createjs.Stage.call(this, canvas);
    }).prototype = p = new createjs.Stage();

    p.setAutoPlay = function(autoPlay) {
        this.tickEnabled = autoPlay;
    }
    p.play = function() {
        this.tickEnabled = true;
        this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
    }
    p.stop = function(ms) {
        if (ms) this.seek(ms);
        this.tickEnabled = false;
    }
    p.seek = function(ms) {
        this.tickEnabled = true;
        this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
    }
    p.getDuration = function() {
        return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
    }

    p.getTimelinePosition = function() {
        return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
    }

    an.bootcompsLoaded = an.bootcompsLoaded || [];
    if (!an.bootstrapListeners) {
        an.bootstrapListeners = [];
    }

    an.bootstrapCallback = function(fnCallback) {
        an.bootstrapListeners.push(fnCallback);
        if (an.bootcompsLoaded.length > 0) {
            for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
                fnCallback(an.bootcompsLoaded[i]);
            }
        }
    };

    an.compositions = an.compositions || {};
    an.compositions['5F627695D751418490F36BAA3DC98329'] = {
        getStage: function() {
            return exportRoot.getStage();
        },
        getLibrary: function() {
            return lib;
        },
        getSpriteSheet: function() {
            return ss;
        },
        getImages: function() {
            return img;
        }
    };

    an.compositionLoaded = function(id) {
        an.bootcompsLoaded.push(id);
        for (var j = 0; j < an.bootstrapListeners.length; j++) {
            an.bootstrapListeners[j](id);
        }
    }

    an.getComposition = function(id) {
        return an.compositions[id];
    }



})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn;
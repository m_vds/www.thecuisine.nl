/* full screen image slider cuisine index.html */
var arr = ['images/hero/cuisine-keukenblad-ingredienten.jpg', 'images/hero/cuisine-nieuws-hero-image.jpg', 'images/hero/hero-slider-3.jpg']; //an array of image sources
var pos = 0; //initializes image position in the array
$(document).ready(function() {
    var interval = 3000; //interval for slide
    var loaderHtml = '';
    arr.forEach(function(src) {
        loaderHtml += '<img src="' + src + '">';
    });

    $('.load-images').html(loaderHtml);

    var htm = '';
    /* initializes the small circles html*/
    for (var i = 0; i < arr.length; i++) {
        htm += '<div id="' + i + '" class="circle" onclick="circleClick(' + i + ')"> </div> ';
    }

    $('#circles').html(htm); //show small circles
    $('#slider').html('<img src="' + arr[0] + '" class="img-slide image-animated"">'); //show first image
    $('#0').css({
        'background': '#418C9F',
        'color': '#418C9F'
    }); //sets the background of the first small circle to black

    /* Auto slides the images with the image sources array given as first argument and interval as second argument */
    function autoSlide(arr, interval) {

            setInterval(function() {
                $('.img-slide').css({
                    'opacity': '.1 !important'
                });
                pos++;
                if (pos > arr.length - 1) {
                    pos = 0;
                }

                $('#slider').html('<img src="' + arr[pos] + '" class="img-slide img' + pos + ' image-animated">'); //shows image
                $('#' + pos).css({
                    'background': '#418C9F',
                    'color': '#418C9F'
                }); //sets background-color of circle representing the current active image to black
                $('#' + (pos - 1)).css({
                    'background': 'transparent',
                    'color': 'transparent'
                }); //sets background-color of circle before active to white
                if (pos == 0) {
                    $('#' + (arr.length - 1)).css({
                        'background': 'transparent',
                        'object-fit': '220% 110%',
                        'color': 'transparent'
                    });
                }
            }, interval);
        }
        /* end of function autoSlide */

    autoSlide(arr, interval); //calls function autoSlide

    /* displays next image */
    function next() {
            if (pos > arr.length - 2) {
                pos = -1;
            }
            $('#slider').html('<img src="' + arr[pos + 1] + '" class="img-slide image-animated">'); //show image
            pos++;

            $('#' + pos).css({
                'background': '#418C9F',
                'color': '#418C9F'
            }); //sets background-color of circle representing the current active image to black
            $('#' + (pos - 1)).css({
                'background': 'transparent',
                'color': 'transparent'
            }); //sets background-color of circle before active to white
            if (pos == 0) {
                $('#' + (arr.length - 1)).css({
                    'background': 'transparent',
                    'color': 'transparent'
                });
            }
        }
        /* end of function next  */

    /* displays previous image */
    function previous() {
            if (pos < 1) {
                pos = arr.length;
            }
            $('#slider').html('<img src="' + arr[pos - 1] + '" class="img-slide image-animated">');
            pos--;

            $('#' + pos).css({
                'background': '#418C9F',
                'color': '#fff'
            }); //sets background-color of circle representing the current active image to black
            $('#' + (pos + 1)).css({
                'background': 'transparent',
                'color': 'transparent'
            }); //sets background-color of circle before active to white
            if (pos == arr.length - 1) {
                $('#0').css({
                    'background': 'transparent',
                    'color': 'transparent'
                });
            }
        }
        /* end of function previous */

    /* onclick next */
    $('button#next').on('click', function(e) {
        e.preventDefault();
        next(); //call function next
    });
    /* end of onclick next */

    /* onclick previous */
    $('button#prev').on('click', function(e) {
        e.preventDefault();
        previous(); //call function previous
    });
    /* end of onclick previous */
});

/* displays image represented by the small circle */
function circleClick(position) {
        if (position != pos) {
            $('#slider').html('<img src="' + arr[position] + '" class="img-slide image-animated">'); //show image

            $('#' + position).css({
                'background': '#418C9F',
                'color': '#418C9F'
            }); //sets background-color of circle representing the current active image to black
            $('#' + (pos)).css({
                'background': 'transparent',
                'color': 'transparent'
            }); //sets background-color of circle before active to white

            pos = position;
        }
        /* end of function circleClick */
    }
    //activate script
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();
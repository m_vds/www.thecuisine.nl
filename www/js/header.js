// hamburger toggle for cuisine header
var hamburger = document.getElementById('hamburger');
var menu = document.getElementById('menu');

hamburger.addEventListener("click", function() {
    menu.classList.toggle('hide');
});

function updateTextInput(val) {
    document.getElementById('sliderValue').innerHTML = val;
}
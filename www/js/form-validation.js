$(document).ready(function() {

    // form validatie cuisine voor account en contact.html

    $('.submit').click(function() {
        validateForm();
    });

    function validateForm() {

        var nameReg = /^[A-Za-z]+$/;
        var numberReg = /^[0-9]+$/;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        var names = $('#nameInput').val();
        var company = $('#companyInput').val();
        var email = $('#emailInput').val();
        var telephone = $('#telInput').val();
        var message = $('#messageInput').val();

        var inputVal = new Array(names, company, email, telephone, message);

        var inputMessage = new Array("name", "company", "email address", "telephone number", "message");

        $('.error').hide();

        if (inputVal[0] == "") {
            $('#nameLabel').after('<span class="error"> Please enter your ' + inputMessage[0] + '</span>');
        } else if (!nameReg.test(names)) {
            $('#nameLabel').after('<span class="error"> Letters only</span>');
        }

        if (inputVal[1] == "") {
            $('#companyLabel').after('<span class="error"> Please enter your ' + inputMessage[1] + '</span>');
        }

        if (inputVal[2] == "") {
            $('#emailLabel').after('<span class="error"> Please enter your ' + inputMessage[2] + '</span>');
        } else if (!emailReg.test(email)) {
            $('#emailLabel').after('<span class="error"> Please enter a valid email address</span>');
        }

        if (inputVal[3] == "") {
            $('#telephoneLabel').after('<span class="error"> Please enter your ' + inputMessage[3] + '</span>');
        } else if (!numberReg.test(telephone)) {
            $('#telephoneLabel').after('<span class="error"> Numbers only</span>');
        }

        if (inputVal[4] == "") {
            $('#messageLabel').after('<span class="error"> Please enter your ' + inputMessage[4] + '</span>');
        }
    }

});

$('#myform').validate({ // initialize the plugin
    rules: { //contact pagina
        naam: {
            required: true,
            minlength: 2
        },
        email: {
            required: true,
            minlength: 9
        },
        message: {
            required: true,
            minlength: 5
        },
        radio2: {
            required: true
        }, //vanaf hier account pagina
        voornaam: {
            required: true,
            minlength: 2
        },
        achternaam: {
            required: true,
            minlength: 2
        },
        adres: {
            required: true,
            minlength: 5
        },
        postcode: {
            required: true,
            minlength: 6
        },
        radio3: {
            required: true
        },
        wachtwoord: {
            required: true,
            minlength: 8
        },
        AantalPersonen: {
            required: true,
            minlength: 1
        },
    }
});
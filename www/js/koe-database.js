//koe database voor cuisine koe.html
var koeDelen = [];
koeDelen.push({}); // Op de eerste plek een leeg object neerzetten, zodat koe #1 op index key #1 staat.

// koe deel 1
var koe = {
    title: "Entrecote",
    synopsis: "Entrecote is een premium gesneden stuk rundvlees van de dunne lende en heeft een klein vetrandje. Entrecote betekent letterlijk vertaald 'tussen de ribben' en werd traditioneel dan ook gesneden uit het ribgedeelte van het rund. Tegenwoordig van het lendestuk, in het Frans contrefilet genoemd.",
    image: "entrecote-js.jpg"
}

koeDelen.push(koe);

// koe deel 4 	
var koe = {
    title: "Pepersteak",
    synopsis: "Steak au poivre of pepersteak is een Frans gerecht dat bestaat uit een biefstuk, bij voorkeur een filet mignon, die bedekt wordt met gebroken peperkorrels en vervolgens wordt gegaard. De peperkorrels vormen een korst op de biefstuk en de prikkelende smaak zorgt voor een complementair (aanvullend) contrast met de rijke smaak van het rundvlees. De korst van de peperkorrels zelf wordt gemaakt door de biefstuk op een bedje van gescheurde zwarte (of gemengde) peperkorrels te leggen. Meestal wordt de biefstuk aangebraden in een hete koekenpan met een kleine hoeveelheid boter.",
    image: "pepersteak-js.jpg"
}

koeDelen.push(koe);

// koe deel 2 		
var koe = {
    title: "Sucadelap",
    synopsis: "Sucadelapjes zijn gesneden van de schouder. Er loopt een peesje door het vlees. Daaraan ontleent de sucadelap zijn naam en zijn typische smaak en structuur. Het peesje lijkt na bereiding op sukade (geconfijt fruit) en is ook eetbaar.",
    image: "sucadelap-js.jpg"
}

koeDelen.push(koe);

// koe deel 3 		
var koe = {
    title: "Biefstuk",
    synopsis: "Biefstuk of steak is een benaming voor mals en mager spiervlees. Biefstuk is van het rund of het kalf. Verschillende soorten runderen geven speciaal rundvlees, bijvoorbeeld: Wagyu en de Black Angus. De naam biefstuk wordt ook wel gebruikt voor vlees van bijvoorbeeld struisvogel, hert, kalkoen, paard, kangoeroe en bizon. De benaming is afkomstig van het Engelse beefsteak dat in vele talen verbasterd voorkomt. Biefstuk behoort tot de meest geliefde vleessoorten en is vrij duur. Kwaliteit en prijs worden hoofdzakelijk bepaald door het deel van de rund waarvan de biefstuk wordt gesneden, waarbij biefstuk van de haas als de meest malse soort geldt en de lendenbiefstuk als de minst malse. Verder maakt de mestwijze, leeftijd en de levenskwaliteit van het rund en het ras van het dier veel uit voor de smaak.",
    image: "biefstuk-js.jpg"
}

koeDelen.push(koe);
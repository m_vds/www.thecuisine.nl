window.onload = function() {

        // uitvoer code voor cuisine koe.html 

        /* Bestaat het object ports en/of bevat het minder dan 2 items? return dan false */
        if (!document.getElementById("koeContent") || !document.getElementById("koeMenu")) return false;
        if (typeof koeDelen == "undefined" || koeDelen.length < 2) return false;

        // ophalen van de areas
        var koeContent = document.getElementById("koeContent");
        var koeDelenElements = document.getElementsByClassName("koeLinks");

        /* een event verbinden aan de 'click' 
        vervolgens met elke click op een andere poort een andere id op halen uit de database file */
        var eventList = ['click'];
        for (var i = 0; i < koeDelenElements.length; i++) {
            koeDelenElements[i].addEventListener('click',
                function(e) {
                    if (e.type == 'click') {
                        e.preventDefault();
                        updatekoeDelen(this.getAttribute("id").substr(1));
                    }
                }
            );
        }

        // De standaard melding weghalen.
        koeContent.innerHTML = "";

        // #portContent article-tag vullen met een divs.
        var dynamicContent = document.createElement("div");
        dynamicContent.setAttribute("class", "dynamicContent");
        dynamicContent.setAttribute("id", "koeContent");

        // #portContent article-tag vullen met een img.
        var dynamicContentImg = document.createElement("img");
        dynamicContentImg.setAttribute("width", "200");
        dynamicContentImg.setAttribute("height", "100");

        // .dynamicText div voor #koeContent
        var dynamicText = document.createElement("div");
        dynamicText.setAttribute("class", "dynamicText");

        // h1 toevoegen aan de div.
        var h1Koe = document.createElement("h1");
        h1Koe.setAttribute("id", "h1Koe");

        // h1 toevoegen aan de div.
        var koeSynopsis = document.createElement("p");
        koeSynopsis.setAttribute("id", "koeSynopsis");

        // het plaatsen van de content via appenchild.
        dynamicContent.appendChild(dynamicContentImg);
        koeContent.appendChild(dynamicContent);
        dynamicText.appendChild(h1Koe);
        dynamicText.appendChild(koeSynopsis);
        dynamicContent.appendChild(dynamicText);

        // het updaten van de content via de database js file
        function updatekoeDelen(whichKoe) {
                h1Koe.textContent = koeDelen[whichKoe].title;
                koeSynopsis.textContent = koeDelen[whichKoe].synopsis;
                dynamicContentImg.src = "../images/artikelen/koe/" + koeDelen[whichKoe].image;
                dynamicContentImg.alt = koeDelen[whichKoe].title;
            }
            // De directory waarin de images zitten.
        var imagesFolder = "../images/artikelen/koe/";

        // Te preloaden images.
        var imagesList = ["biefstuk-js.jpg", "entrecote-js.jpg", "pepersteak-js.jpg", "sucadelap-js.jpg", ];

        // Ga door de imagesList array heen.
        function preloadImages() {
                for (var i = 0; i < imagesList.length; i++) {

                    // Image constructor van JavaScript, waarmee we nu dynamisch een img tag in het leven roepen.
                    var temp = new Image();

                    // Geef de nieuwe image een src-attribute. De browser gaat nu direct de image opvragen bij de server!
                    temp.src = imagesFolder + imagesList[i];
                }
            }
            // Download afbeeldingen om te preloaden.
        preloadImages();

    } // Einde window.onload